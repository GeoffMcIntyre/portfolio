import { logger } from '@gmp-debug';

import { Router } from 'express';

import * as metrics from './metrics';

import * as ads from './ads';
import * as login from './login';
import * as logout from './logout';
import * as programmingLanguages from './programming-languages';
import * as projectPages from './project-pages';
import * as projects from './projects';
import * as software from './software';
import * as tokens from './tokens';
import * as users from './users';

async function init(router: Router): Promise<void> {
    logger.info("Initialising All Routes...");

    await metrics.init(router);
    
    await ads.init(router);
    await programmingLanguages.init(router);
    await projectPages.init(router);
    await projects.init(router);
    await software.init(router);

    await users.init(router);
    await login.init(router);
    await logout.init(router);
    await tokens.init(router);
}

export {
    init,
    ads
};