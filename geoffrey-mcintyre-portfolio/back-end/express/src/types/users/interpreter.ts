import type { User, UserClean } from '@gmp-types/users';

async function getUserClean(userDirty: User): Promise<UserClean | undefined>{
    if (userDirty) {
        return {
            username: userDirty.username,
            permissionLevel: 0,
            firstName: userDirty.firstName,
            lastName: userDirty.lastName,
            email: userDirty.email
        };
    }
}

export {
    getUserClean
}