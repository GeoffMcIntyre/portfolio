#!/bin/bash

# Admin User
MONGODB_ADMIN_USER=${MONGODB_ADMIN_USER:-"admin"}
MONGODB_ADMIN_PASS=${MONGODB_ADMIN_PASS:-"hq35478q009g0httgh934ghtgh8ogbvofgh34h"}

# Portfolio Site Components User
MONGODB_SITE_COMPONENTS_DATABASE=${MONGODB_SITE_COMPONENTS_DATABASE:-"admin"}
MONGODB_SITE_COMPONENTS_USER=${MONGODB_SITE_COMPONENTS_USER:-"user_sc"}
MONGODB_SITE_COMPONENTS_PASS=${MONGODB_SITE_COMPONENTS_PASS:-"80hb54g83457gtfegfhow7ugf4wehg90we8hgbn"}

# Portfolio User Generated User
MONGODB_USER_ACCOUNTS_DATABASE=${MONGODB_USER_ACCOUNTS_DATABASE:-"admin"}
MONGODB_USER_ACCOUNTS_USER=${MONGODB_USER_ACCOUNTS_USER:-"user_ua"}
MONGODB_USER_ACCOUNTS_PASS=${MONGODB_USER_ACCOUNTS_PASS:-"80hb54g83457gtfegfhow7ugf4wehg90we8hgbn"}

# Portfolio User Generated User
MONGODB_USER_GENERATED_DATABASE=${MONGODB_USER_GENERATED_DATABASE:-"admin"}
MONGODB_USER_GENERATED_USER=${MONGODB_USER_GENERATED_USER:-"user_ug"}
MONGODB_USER_GENERATED_PASS=${MONGODB_USER_GENERATED_PASS:-"80hb54g83457gtfegfhow7ugf4wehg90we8hgbn"}

# Wait for MongoDB to boot
RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MongoDB service startup..."
    sleep 5
    mongo admin --eval "help" >/dev/null 2>&1
    RET=$?
done

# Create the admin user
echo "=> Creating admin user with a password in MongoDB"
mongo admin --eval "db.createUser({user: '$MONGODB_ADMIN_USER', pwd: '$MONGODB_ADMIN_PASS', roles:[{role:'root',db:'admin'}]});"

sleep 3

# If we've defined the MONGODB_SITE_COMPONENTS_DATABASE environment variable and it's a different database
# than admin, then create the user for that database.
# First it authenticates to Mongo using the admin user it created above.
# Then it switches to the REST API database and runs the createUser command 
# to actually create the user and assign it to the database.
if [ "$MONGODB_SITE_COMPONENTS_DATABASE" != "admin" ]; then
    echo "=> Creating a ${MONGODB_SITE_COMPONENTS_DATABASE} database user with a password in MongoDB"
    mongo admin -u $MONGODB_ADMIN_USER -p $MONGODB_ADMIN_PASS << EOF
echo "Using $MONGODB_SITE_COMPONENTS_DATABASE database"
use $MONGODB_SITE_COMPONENTS_DATABASE
db.createUser({user: '$MONGODB_SITE_COMPONENTS_USER', pwd: '$MONGODB_SITE_COMPONENTS_PASS', roles:[{role:'dbOwner', db:'$MONGODB_SITE_COMPONENTS_DATABASE'}]})
EOF
fi

sleep 3

# If we've defined the MONGODB_USER_ACCOUNTS_DATABASE environment variable and it's a different database
# than admin, then create the user for that database.
# First it authenticates to Mongo using the admin user it created above.
# Then it switches to the REST API database and runs the createUser command 
# to actually create the user and assign it to the database.
if [ "$MONGODB_USER_ACCOUNTS_DATABASE" != "admin" ]; then
    echo "=> Creating a ${MONGODB_USER_ACCOUNTS_DATABASE} database user with a password in MongoDB"
    mongo admin -u $MONGODB_ADMIN_USER -p $MONGODB_ADMIN_PASS << EOF
echo "Using $MONGODB_USER_ACCOUNTS_DATABASE database"
use $MONGODB_USER_ACCOUNTS_DATABASE
db.createUser({user: '$MONGODB_USER_ACCOUNTS_USER', pwd: '$MONGODB_USER_ACCOUNTS_PASS', roles:[{role:'dbOwner', db:'$MONGODB_USER_ACCOUNTS_DATABASE'}]})
EOF
fi

sleep 3

# If we've defined the MONGODB_USER_GENERATED_DATABASE environment variable and it's a different database
# than admin, then create the user for that database.
# First it authenticates to Mongo using the admin user it created above.
# Then it switches to the REST API database and runs the createUser command 
# to actually create the user and assign it to the database.
if [ "$MONGODB_USER_GENERATED_DATABASE" != "admin" ]; then
    echo "=> Creating a ${MONGODB_USER_GENERATED_DATABASE} database user with a password in MongoDB"
    mongo admin -u $MONGODB_ADMIN_USER -p $MONGODB_ADMIN_PASS << EOF
echo "Using $MONGODB_USER_GENERATED_DATABASE database"
use $MONGODB_USER_GENERATED_DATABASE
db.createUser({user: '$MONGODB_USER_GENERATED_USER', pwd: '$MONGODB_USER_GENERATED_PASS', roles:[{role:'dbOwner', db:'$MONGODB_USER_GENERATED_DATABASE'}]})
EOF
fi

sleep 1

# If everything went well, add a file as a flag so we know in the future to not re-create the
# users if we're recreating the container (provided we're using some persistent storage)
touch /data/db/.mongodb_password_set

echo "MongoDB configured successfully. You may now connect to the DB."