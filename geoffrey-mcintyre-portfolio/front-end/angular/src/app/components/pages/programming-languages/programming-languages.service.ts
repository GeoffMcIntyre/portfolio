import { Injectable } from '@angular/core';
import { DataService } from '@gmp-services/data.service';
import { Observable, of } from 'rxjs';

@Injectable()
export class ProgrammingLanguagesService {

	constructor(private dataService: DataService) { 
		
	}
	
	async getProgrammingLanguages(): Promise<any[]>{
		return await this.dataService.sendGetRequest('programming-languages');
	}
}