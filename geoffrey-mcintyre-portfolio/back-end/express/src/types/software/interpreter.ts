import type { Software, SoftwareClean } from '@gmp-types/software';

async function getSoftwareClean(softwareDirty: Software): Promise<SoftwareClean | undefined>{
    if (softwareDirty) {
        return {
            name: softwareDirty.name,
            description: softwareDirty.description
        };
    }
}

export {
    getSoftwareClean
}