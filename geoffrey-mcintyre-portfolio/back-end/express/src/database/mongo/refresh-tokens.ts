import { errorHandler, logger, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';
import { DatabaseInterface } from '@gmp-classes/databaseInterface';
import { jwtSignUserNewAuth, jwtVerifyUserRefresh } from '@gmp-services/crypto';

import type { Db } from 'mongodb';
import type { JwtToken }  from '@gmp-types/crypto';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from '@gmp-types/database';
import type { RefreshToken } from '@gmp-types/refresh-tokens';
import type { User, UserClean, UserDBEntry } from '@gmp-types/users';

const collectionProperties: CollectionProperties = {
    collectionName: 'refresh-tokens',
    collectionSchema: {
        validator: {
            $jsonSchema: {
                bsonType: "object",
                required: [ "username", "token" ],
                additionalProperties: false,
                properties: {
                    _id: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    },
                    username: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    token: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    }
                }
            }
        }
    },
    collectionIndexes: [
        {
            "token": 1
        }
    ],
    collectionIndexProperties: {
        unique: true,
        name: "query for refresh tokens"
    }
};

class RefreshTokensDatabaseInterface extends DatabaseInterface {
    constructor(db: Db, collectionProperties: CollectionProperties) {
        super(db, collectionProperties);
    }

    /**
     * Public
     */
    
    public async insert(refreshToken: RefreshToken): Promise<DBInsertResultClean> {
        return await super.insert(refreshToken);
    }
    
    public async logout(refreshToken: string): Promise<DBDeleteResultClean> {
        const result = await super._removeBy('token', refreshToken);

        if (result.success) {
            return {
                success: true,
                message: `Logout Successful`
            };
        }
        
        return {
            success: false,
            message: `Logout Failed`
        };
    }
    
    public async logoutAll(username: string): Promise<DBDeleteResultClean> {
        const result = await super._removeBy('username', username);

        if (result.success) {
            return {
                success: true,
                message: `Logout Successful`
            };
        }
        
        return {
            success: false,
            message: `Logout Failed`
        };
    }

    /**
     * Security
     */
    
    public async generateNewAuthToken(refreshToken: string): Promise<DBGetResultClean> {
        const result = await this._getBy('token', refreshToken);
        
        if (result.success) {
            try {
                //const foundTokenData = (result.data as RefreshToken);
                const verifiedUser = jwtVerifyUserRefresh(refreshToken);

                if (verifiedUser) {
                    const newAuthToken = await jwtSignUserNewAuth(verifiedUser);

                    return {
                        success: true,
                        message: `Generated New Authentication Token`,
                        data: newAuthToken
                    };
                }
            } catch (err) {
                errorHandler.catchAuthenticationError(err, 'Issue Generating New Authentication Token');
            }
        }

        return {
            success: false,
            message: `Unable to Generate New Authentication Token`
        };
    }
}

let databaseInterface: RefreshTokensDatabaseInterface;

async function init(): Promise<void> {
    databaseInterface = new RefreshTokensDatabaseInterface(await db.getSiteComponentsDb(), collectionProperties);
}

async function getDatabaseInterface(): Promise<RefreshTokensDatabaseInterface> {
    if (!databaseInterface) await init();

    return databaseInterface;
}

export {
    RefreshTokensDatabaseInterface,
    getDatabaseInterface
};