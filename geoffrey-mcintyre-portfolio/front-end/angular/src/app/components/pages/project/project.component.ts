import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ComponentFunctionsService } from '@gmp-services/component-functions.service';
import { ComponentGeneratorService } from '@gmp-services/component-generator.service';
import { ComponentGeneratorDirective } from '@gmp-directives/component-generator.directive';

import { ProjectService } from './project.service';

import type { Project } from '@gmp-types/projects';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
	@ViewChild(ComponentGeneratorDirective, {static: true}) componentHost?: ComponentGeneratorDirective;

	public title = 'Geoffs Portfolio - Project';

	public projectId: string = '' as string;
	public project: Project = {} as Project;
  
	public constructor(
		private activatedRoute: ActivatedRoute,
		private componentFunctionsService: ComponentFunctionsService,
		private componentGeneratorService: ComponentGeneratorService,
		private projectService: ProjectService
	) {
		componentFunctionsService.setTitle(this.title);
	}

	ngOnInit(): void {
		this.generateProjectContent();
	}

	async generateProjectContent(): Promise<void>{
		this.projectId = await this.getProjectId();
		this.project = await this.getProject(this.projectId);

		this.buildContent(this.project);
	}

	async getProjectId(): Promise<string>{
		return new Promise(resolve => {
			this.activatedRoute.paramMap.subscribe(params => {
				const tempProjectId = params.get('projectId');
	
				if (tempProjectId) {
					resolve(tempProjectId.toString());
				}
	
				resolve('');
			});
		});
	}

	async getProject(projectId: string): Promise<Project>{
		const data: Project = await this.projectService.getProject(projectId);

		console.log("Project", data);
		
		return data;
	}

	async buildContent(project: Project): Promise<void> {
		if (project && project.page && this.componentHost) {
			this.componentGeneratorService.buildContent(this.componentHost, project.page);
		}
	}
}
