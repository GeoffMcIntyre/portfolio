export interface ProgrammingLanguage {
	_id: string;
	name: string;
	description: string;
}