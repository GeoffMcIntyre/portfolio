import type { Ad, AdClean } from "./ads";
import type { JwtToken, JwtTokens }  from '@gmp-types/crypto';
import type { Language, LanguageClean } from "./languages";
import type { Page, PageClean } from "./pages";
import type { ProgrammingLanguage, ProgrammingLanguageClean } from "./programming-languages";
import type { Project, ProjectClean } from "./projects";
import type { RefreshToken } from "./refresh-tokens";
import type { Software, SoftwareClean } from "./software";
import type { User, UserClean } from "./users";

export type PotentialDataTypesClean =
    AdClean |
    JwtToken |
    JwtTokens |
    LanguageClean |
    PageClean |
    ProgrammingLanguageClean |
    ProjectClean |
    SoftwareClean |
    UserClean;

export type PotentialDataTypes =
    Ad |
    Language |
    Page |
    ProgrammingLanguage |
    Project |
    RefreshToken |
    Software |
    User;

export interface DBGetResult {
    success: boolean;
    message: string;
    data?: PotentialDataTypes[] | PotentialDataTypes;
}

export interface DBGetResultClean {
    success: boolean;
    message: string;
    data?: PotentialDataTypesClean[] | PotentialDataTypesClean;
}

export interface DBInsertResult {
    success: boolean;
    message: string;
    data?: DBInsertResultData;
}

export interface DBInsertResultClean {
    success: boolean;
    message: string;
    data?: DBInsertResultData;
}

export interface DBUpdateResult {
    success: boolean;
    message: string;
}

export interface DBUpdateResultClean {
    success: boolean;
    message: string;
}

export interface DBDeleteResult {
    success: boolean;
    message: string;
}

export interface DBDeleteResultClean {
    success: boolean;
    message: string;
}

export interface DBInsertResultData {
    id?: string;
    ids?: string[];
    count: number;
}