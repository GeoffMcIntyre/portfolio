import 'module-alias/register';

import { errorHandler, logger, nodeErrorHandler } from '@gmp-debug';
logger.info('Starting API...');

import * as metrics from '@gmp-services/metrics';
metrics.start();

// Import the dependencies
import express, { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import morgan from 'morgan';

import { startAll as startDatabases } from '@gmp-mongo';
import * as routes from '@gmp-routes/all';

import * as config from '@gmp-config';

// Start Database
startDatabases().then(async () => {
    const port = config.get().httpPort;
    
    // Define the Express Server
    const server = express();
    
    // Add Helmet to enhance your API's security
    server.use(helmet());
    
    // Use bodyParser to parse JSON bodies into JS objects
    server.use(bodyParser.json());
    
    // Add morgan to log HTTP requests
    server.use(morgan('combined'));
    
    // Initialise Routes
    await routes.init(server._router);
    
    // Catch all requests that don't exist
    server.all('*', (req:Request, res:Response, next:NextFunction) => {
        next(new errorHandler.AppError(`Can't find ${req.originalUrl}`, 404));
    });
    
    // Catch all errors
    server.use(async (err: errorHandler.AppError, req: Request, res: Response, next: NextFunction) => errorHandler.catchError(err, req, res, next));
    
    // Handle all errors
    nodeErrorHandler.listen();

    // Start server
    server.listen(port, async () => {
        logger.info(`API listening on port \`${port}\``);
    });
});