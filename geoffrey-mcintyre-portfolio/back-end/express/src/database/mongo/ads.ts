import { errorHandler, logger, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';
import { DatabaseInterface } from '@gmp-classes/databaseInterface';
import { getAdClean } from '@gmp-types/ads/interpreter';
import { sanitiseAd } from '@gmp-types/ads/sanitisation';

import type { Db } from 'mongodb';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from '@gmp-types/database';
import type { Ad, AdClean } from '@gmp-types/ads';

const collectionProperties: CollectionProperties = {
    collectionName: 'ads',
    collectionSchema: {
        validator: {
            $jsonSchema: {
                bsonType: "object",
                required: ["title", "price"],
                additionalProperties: false,
                properties: {
                    _id: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    },
                    title: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    price: {
                        bsonType: ["double"],
                        description: "must be a double if the field exists"
                    }
                }
            }
        }
    },
    collectionIndexes: [
        {
            "title": 1
        }
    ],
    collectionIndexProperties: {
        unique: true,
        name: "query for ads"
    }
};

class AdsDatabaseInterface extends DatabaseInterface {
    constructor(db: Db, collectionProperties: CollectionProperties) {
        super(db, collectionProperties);
    }

    /**
     * Protected
     */

    /**
    * Clean
    */
    
    protected async _cleanGetResult(dirtyResult: DBGetResult): Promise<DBGetResultClean> {
        const cleanResult: DBGetResultClean = {
            success: dirtyResult.success,
            message: dirtyResult.message
        };

        if (Array.isArray(dirtyResult.data)) {
            const ads = (dirtyResult.data as Ad[]);
            cleanResult.data = [];

            for (let i = 0; i < ads.length; i++){
                const adClean = await getAdClean(ads[i]);

                if (adClean) {
                    cleanResult.data.push(adClean);
                }
            }
        } else {
            const adClean = await getAdClean((dirtyResult.data as Ad));

            if (adClean) {
                cleanResult.data = adClean;
            }
        }

        return cleanResult;
    }

    /**
     * Public
     */

    public async getAll(): Promise<DBGetResultClean> {
        return await super.getAll();
    }

    public async getById(id: string): Promise<DBGetResultClean> {
        return await super.getById(id);
    }
    
    public async getByTitle(name: string): Promise<DBGetResultClean> {
        return await super.getByTitle(name);
    }
    
    public async insert(ad: Ad): Promise<DBInsertResultClean> {
        ad = await sanitiseAd(ad);

        const existing = await this._getByTitle(ad.title);
        
        if (existing.success) {
            warningHandler.throwDatabaseWarning(`Ad \`${ad.title}\` already exists`);

            return {
                success: false,
                message: `Ad \`${ad.title}\` already exists`
            };
        }
        
        return await super.insert(ad);
    }
    
    public async insertMultiple(ads: Ad[]): Promise<DBInsertResultClean> {
        return await super.insertMultiple(ads);
    }
    
    public async update(id: string, ad: Ad): Promise<DBUpdateResultClean> {
        ad = await sanitiseAd(ad);

        return await super.update(id, ad);
    }
    
    public async remove(id: string): Promise<DBDeleteResultClean> {
        return await super.remove(id);
    }
}

let databaseInterface: AdsDatabaseInterface;

async function init(): Promise<void> {
    databaseInterface = new AdsDatabaseInterface(await db.getSiteComponentsDb(), collectionProperties);
}

async function getDatabaseInterface(): Promise<AdsDatabaseInterface> {
    if (!databaseInterface) await init();

    return databaseInterface;
}

export {
    AdsDatabaseInterface,
    getDatabaseInterface
};