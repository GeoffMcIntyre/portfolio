export interface ConfigEnvironment {
    httpPort: string | number;
    httpAddress: string;
    envName: string;
    log: ConfigLog;
    databases: ConfigDatabases;
    jwt: ConfigJwt;
}

export interface ConfigLog {
    level: string | number;
}

export interface ConfigDatabases {
    mongo?: ConfigDatabaseType;
    redis?: ConfigDatabaseType;
}

export interface ConfigDatabaseType {
    cache?: ConfigDatabase;
    siteComponents?: ConfigDatabase;
    userAccounts?: ConfigDatabase;
    userGenerated?: ConfigDatabase;
}

export interface ConfigDatabase {
    url: string | undefined;
    port?: string | number;
    name?: string;
    connectRetry?: number;
}

export interface ConfigJwt {
    secret: string | undefined;
    refreshSecret: string | undefined;
}