import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentGeneratorDirective } from '@gmp-directives/component-generator.directive';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
	@ViewChild(ComponentGeneratorDirective, {static: true}) componentHost?: ComponentGeneratorDirective;

	constructor() { }

	ngOnInit(): void {

	}
  
	async getComponentHost(): Promise<ComponentGeneratorDirective>{
		return new Promise(resolve => {
			if (this.componentHost) {
				resolve(this.componentHost);
			}
		});
	}
}
