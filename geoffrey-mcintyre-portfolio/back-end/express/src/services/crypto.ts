import { errorHandler } from '@gmp-debug';

import { JwtToken, JwtTokens } from '@gmp-types/crypto';
import { UserClean } from '@gmp-types/users';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import * as config from '@gmp-config';

const saltRounds = 10;
const JWT_SECRET = config.get().jwt.secret;
const JWT_REFRESH_SECRET = config.get().jwt.refreshSecret;

async function bcryptHash(password: string, callback: (err: Error, hash: string) => void): Promise<void> {
    bcrypt.hash(password, saltRounds, function (err: Error, hash: string) {
        callback(err, hash);
    });
}

async function bcryptCompare(password: string, hash: string, callback: (err: Error, result: boolean) => void): Promise<boolean | Error> {
    return bcrypt.compare(password, hash, function(err: Error, match: boolean) {
        callback(err, match);
    });
}

async function jwtSignUser(userClean: UserClean): Promise<JwtTokens> {
    if (JWT_SECRET && JWT_REFRESH_SECRET) {
        const token = jwt.sign(userClean, JWT_SECRET, { expiresIn: '1h' });
        const refreshToken = jwt.sign(userClean, JWT_REFRESH_SECRET);

        const jwtTokens: JwtTokens = {
            token: token,
            refreshToken: refreshToken
        };

        return jwtTokens;
    }
    
    throw (new errorHandler.AppError('Unable to sign user'));
}

async function jwtSignUserNewAuth(userClean: UserClean): Promise<JwtToken> {
    if (JWT_SECRET) {
        const token = jwt.sign(userClean, JWT_SECRET, { expiresIn: '1h' });

        const jwtToken: JwtToken = {
            token: token
        };
    
        return jwtToken;
    }
    
    throw (new errorHandler.AppError('Unable to sign user'));
}

function jwtVerifyUser(token: string): UserClean {
    if (JWT_SECRET) {
        return jwt.verify(token, JWT_SECRET) as UserClean;
    }
    
    throw (new errorHandler.AppError('Unable to verify user'));
}

function jwtVerifyUserRefresh(token: string): UserClean {
    if (JWT_REFRESH_SECRET) {
        return jwt.verify(token, JWT_REFRESH_SECRET) as UserClean;
    }
    
    throw (new errorHandler.AppError('Unable to verify user'));
}

export {
    bcryptHash,
    bcryptCompare,
    jwtSignUser,
    jwtSignUserNewAuth,
    jwtVerifyUserRefresh,
    jwtVerifyUser
}