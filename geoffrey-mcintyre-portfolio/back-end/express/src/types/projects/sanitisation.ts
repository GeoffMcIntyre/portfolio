import { sanitiseString } from '@gmp-services/sanitisation';

import { sanitisePage } from '@gmp-types/pages/sanitisation';
import { sanitiseProgrammingLanguages } from '@gmp-types/programming-languages/sanitisation';
import { sanitiseReferences } from '@gmp-types/references/sanitisation';
import { sanitiseSoftwares } from '@gmp-types/software/sanitisation';

import type { Project, ProjectDates, ProjectDBEntry } from '@gmp-types/projects';

async function sanitiseProject(projectDirty: Project): Promise<Project> {
    return {
        prettyId: await sanitiseString(projectDirty.prettyId),
        name: await sanitiseString(projectDirty.name),
        dates: await sanitiseProjectDates(projectDirty.dates),
        description: await sanitiseString(projectDirty.description),
        programmingLanguages: await sanitiseProgrammingLanguages(projectDirty.programmingLanguages),
        software: await sanitiseSoftwares(projectDirty.software),
        page: projectDirty.page ? await sanitisePage(projectDirty.page) : undefined
    };
}

async function sanitiseProjectDBEntry(projectDBEntryDirty: ProjectDBEntry): Promise<ProjectDBEntry> {
    return {
        prettyId: await sanitiseString(projectDBEntryDirty.prettyId),
        name: await sanitiseString(projectDBEntryDirty.name),
        dates: await sanitiseProjectDates(projectDBEntryDirty.dates),
        description: await sanitiseString(projectDBEntryDirty.description),
        programmingLanguages: await sanitiseReferences(projectDBEntryDirty.programmingLanguages),
        software: await sanitiseReferences(projectDBEntryDirty.software),
        pageId: await sanitiseString(projectDBEntryDirty.pageId)
    };
}

async function sanitiseProjectDates(projectDatesDirty: ProjectDates): Promise<ProjectDates> {
    return {
        from: projectDatesDirty.from,
        to: projectDatesDirty.to
    };
}

export {
    sanitiseProject,
    sanitiseProjectDBEntry
}