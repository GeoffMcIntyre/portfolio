import { sanitiseString } from '@gmp-services/sanitisation';

import type { Software } from '@gmp-types/software';

async function sanitiseSoftware(softwareDirty: Software): Promise<Software>{
    return {
        name: await sanitiseString(softwareDirty.name),
        description: await sanitiseString(softwareDirty.description)
    };
}

async function sanitiseSoftwares(softwareDirty: Software[]): Promise<Software[]>{
    const softwareClean: Software[] = [];

    for (let i = 0; i < softwareDirty.length; i++){
        softwareClean.push({
            name: await sanitiseString(softwareDirty[i].name),
            description: await sanitiseString(softwareDirty[i].description)
        })
    }

    return softwareClean;
}

export {
    sanitiseSoftware,
    sanitiseSoftwares
}