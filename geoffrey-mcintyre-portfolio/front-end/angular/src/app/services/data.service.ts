import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private REST_API_SERVER = "/api/";

    constructor(private httpClient: HttpClient) { }

    public sendGetRequest(path: string): Promise<any[] | any>{
		return new Promise(resolve => {
            this.httpClient.get<any>(this.REST_API_SERVER + path).subscribe((response: any) => {
                console.log("RESPONSE: ", response);

                if ('result' in response && 'data' in response.result) {
                    resolve(response.result.data);
                }
    
                resolve({} as any);
            });
		});
    }
}