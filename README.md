# Geoffrey Mcintyre's Portfolio

## Docker

### References
https://github.com/vegasbrianc/prometheus
https://docs.docker.com/engine/swarm/stack-deploy/
https://medium.com/@basi/docker-swarm-metrics-in-prometheus-e02a6a5745a
https://github.com/nginx-proxy/nginx-proxy
https://medium.com/faun/how-to-backup-docker-containered-mongo-db-with-mongodump-and-mongorestore-b4eb1c0e7308

### Commands

- Allow script execution:       Set-ExecutionPolicy -Scope Process -ExecutionPolicy RemoteSigned

- List running containers:      docker ps

- Init swarm:                   docker swarm init
- Exit swarm:                   docker swarm leave --force

- Add registry service:         docker service create --name registry --publish published=5000,target=5000 registry:2
- Remove registry service:      docker service rm registry
- Remove services from stack:   docker stack rm gmp

- Build project:                docker-compose build
- Boot project:                 docker-compose up
- Push project to service:      docker-compose push
- Add project name:             docker-compose up -p gmp

- Deploy monitoring to stack:   docker stack deploy -c docker-monitor-network-stack.yml gmp
- Deploy project to stack:      docker stack deploy -c docker-compose.yml gmp
- List running services:        docker service ls
- Follow logs from service:     docker service logs gmp_gmp-back-end-express-api --follow

- Access image:                 docker exec -ti [IMAGE-NAME] sh

### Guide

#### Initialise Docker Swarm Stack

1. Init swarm.
2. Add registry service. This is used to push our project to the stack. The address of 127.0.0.1:5000/ is attached to their image path for this reason.
3. Deploy monitoring network.
4. Build project.
5. Boot project.
6. Push project to service.
7. Deploy project to stack.
8. List running services.

1. Remove services from stack.

#### Compose multiple

docker-compose -f docker-compose.yml -f docker-compose.monitor-network.yml up -d

#### Access an Image

docker exec -ti gmp_mongoDB.spybqjo9qc230n1bh64zw41bh.yxpgbzgnlwlf15ekw2pg3e870 sh

## MongoDB

### Commands

- Backup:                       docker exec -i [IMAGE-NAME] mongodump --archive --gzip --db [DATABASE-NAME] > backup.gz
- Restore:                      docker exec -i [IMAGE-NAME] mongorestore --archive --gzip < backup.gz

- Backup:                       docker-compose exec -T [IMAGE-NAME] mongodump --archive --gzip --db [DATABASE-NAME] > backup.gz
- Restore:                      docker-compose exec -T [IMAGE-NAME] mongorestore --archive --gzip < backup.gz

### Guide

Use name from 'docker ps' output to get name.

### Examples

- Backup:                       docker exec -i gmp_mongoDB.spybqjo9qc230n1bh64zw41bh.yxpgbzgnlwlf15ekw2pg3e870 mongodump --archive --gzip --db geoffrey-mcintyre-portfolio > mongodb-backup.gz
- Restore:                      docker exec -i gmp_mongoDB.spybqjo9qc230n1bh64zw41bh.yxpgbzgnlwlf15ekw2pg3e870 mongorestore --archive --gzip < mongodb-backup.gz


- cd geoffrey-mcintyre-portfolio/front-end/angular
- cd geoffrey-mcintyre-portfolio/back-end/express

- docker service logs gmp_gmp-front-end-angular-app --follow
- docker service logs gmp_gmp-back-end-express-api --follow


docker inspect -f '{{range.NetworkSettings.Networks}} {{.IPAddress}} {{end}}' gmp_mongoDB.spybqjo9qc230n1bh64zw41bh.2fchpaupagva8pvzasfsqlape

docker service rm gmp_mongoDB

docker secret create DB_PASSWORD db_password.txt