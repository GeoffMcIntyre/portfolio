import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appComponentGenerator]'
})
export class ComponentGeneratorDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
