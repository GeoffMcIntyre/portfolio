
import { logger, errorHandler } from '@gmp-debug';
     
import cache from 'express-redis-cache';
import * as config from '@gmp-config';

const redisURL = config.get().databases.redis?.cache?.url;
const redisPort = config.get().databases.redis?.cache?.port;

let client1: cache.ExpressRedisCache;

function start(): void {
    client1 = cache(
        {
            host: redisURL,
            port: redisPort
        }
    );

    client1.on('error', function (err) {
        errorHandler.catchCacheError(err);
    });

    client1.on('message', function (message) {
        logger.info(`Cache Info: ${message}`);
    });

    client1.on('connected', function () {
        logger.info(`Cache Connected`);
    });

    client1.on('disconnected', function () {
        logger.info(`Cache Disconnected`);
    });

    client1.del('*', function () {
        logger.info(`Cache Cleared`);
    })
}

function get(): cache.ExpressRedisCache {
    if (!client1) start();
    return client1;
}

export {
    start,
    get
}