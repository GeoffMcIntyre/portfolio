import { NextFunction, Request, Response, Router } from 'express';

import { getRouteController } from '@gmp-controllers/users';

import { Route } from '@gmp-classes/route';

async function init(router: Router): Promise<void> {
    const route = new Route(router, 'login');
    const routeController = await getRouteController();
    
    route.addPost({
        authenticationOptions: {
            disabled: true
        },
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.login(req, res, next)
    });
}

export {
    init
};