import { NextFunction, Request, Response, Router } from 'express';

import { getRouteController } from '@gmp-controllers/ads';

import { Route } from '@gmp-classes/route';

async function init(router: Router): Promise<void> {
    const route = new Route(router, 'ads');
    const routeController = await getRouteController();

    route.addGet({
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.getAll(req, res, next)
    });

    route.addGet({
        path: ':id',
        authenticationOptions: {
            disabled: true
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.getById(req, res, next)
    });

    route.addPost({
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.insert(req, res, next)
    });

    route.addPut({
        path: ':id',
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.update(req, res, next)
    });

    route.addDelete({
        path: ':id',
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.remove(req, res, next)
    });
}

export {
    init
};