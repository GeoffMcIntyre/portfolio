export interface ProgrammingLanguage {
	_id?: string;
	name: string;
	description: string;
}

export interface ProgrammingLanguageClean {
	name: string;
	description: string;
}

export interface DBGetProgrammingLanguageResult {
    success: boolean;
    message: string;
	data: ProgrammingLanguageClean[] | ProgrammingLanguageClean;
}