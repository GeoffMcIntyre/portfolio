import { Injectable, ComponentFactory, ComponentFactoryResolver, ComponentRef, OnInit, OnDestroy, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentGeneratorDirective } from '@gmp-directives/component-generator.directive';

import { ContainerComponent } from '@gmp-c-general/container/container.component';
import { ParagraphComponent } from '@gmp-c-general/paragraph/paragraph.component';

import type {
	ComponentTypes,
	Container,
	ContainerTypes,
	Page,
    Paragraph
} from '@gmp-types/pages';

type availableDirectives = ComponentGeneratorDirective;

@Injectable({
  providedIn: 'root'
})
export class ComponentGeneratorService {

	constructor(
		private componentFactoryResolver: ComponentFactoryResolver
	) { }

	public async buildContent(hostDirective: availableDirectives, page: Page): Promise<void> {
		const viewContainerRef:ViewContainerRef = hostDirective.viewContainerRef;
		viewContainerRef.clear();

		if (page && page.containers) {
			for (let i = 0; i < page.containers.length; i++){
				const container = page.containers[i];

				const containerHostDirective: (availableDirectives | null) = await this.addContainer(hostDirective, container);

				if (containerHostDirective && container.components) {
					for (let j = 0; j < container.components.length; j++) {
						const component = container.components[j];

						if (component) {
							this.addComponent(containerHostDirective, component);
						}
					}
				}
			}
		}
	}

	public async addContainer(hostDirective:availableDirectives, container: ContainerTypes): Promise<availableDirectives | null> {
		if (container) {
			if (container.type == 'container') {
				return await this.buildContainer(hostDirective, container as Container);
			}
		}

		return null;
	}
	
	public async buildContainer(hostDirective:availableDirectives, container: Container): Promise<availableDirectives | null> {
		if (hostDirective) {
			const componentFactory:ComponentFactory<ContainerComponent> = this.componentFactoryResolver.resolveComponentFactory(ContainerComponent);

			const viewContainerRef:ViewContainerRef = hostDirective.viewContainerRef;
			//viewContainerRef.clear();
			
			const componentRef:ComponentRef<ContainerComponent> = viewContainerRef.createComponent<ContainerComponent>(componentFactory);
			
			return await componentRef.instance.getComponentHost();
		}

		return null;
	}

	public async addComponent(hostDirective:availableDirectives, component: ComponentTypes): Promise<void> {
		if (component) {
			if (component.type == 'p') {
				this.buildParagraph(hostDirective, component as Paragraph);
			}
		}
	}
	
	public async buildParagraph(hostDirective:availableDirectives, paragraph: Paragraph): Promise<void> {
		if (hostDirective) {
			const componentFactory:ComponentFactory<ParagraphComponent> = this.componentFactoryResolver.resolveComponentFactory(ParagraphComponent);

			const viewContainerRef:ViewContainerRef = hostDirective.viewContainerRef;
			//viewContainerRef.clear();
			
			const componentRef:ComponentRef<ParagraphComponent> = viewContainerRef.createComponent<ParagraphComponent>(componentFactory);
			
			componentRef.instance.data = {
				text: paragraph.text
			};
		}
	}
}
