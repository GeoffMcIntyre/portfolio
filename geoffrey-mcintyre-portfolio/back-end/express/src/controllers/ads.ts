import { getDatabaseInterface as db } from '@gmp-mongo/ads';

import { RouteController } from '@gmp-classes/routeController';

let adsRouteController: RouteController

async function init(): Promise<void> {
    adsRouteController = new RouteController(await db());
}

async function getRouteController(): Promise<RouteController> {
    if (!adsRouteController) await init();

    return adsRouteController;
}

export {
    getRouteController
};