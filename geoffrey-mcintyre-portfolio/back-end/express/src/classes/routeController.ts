import { errorHandler, logger } from '@gmp-debug';

import { NextFunction, Request, Response } from 'express';

import { DatabaseInterface } from '@gmp-classes/databaseInterface';

import type { PotentialRouteControllerResults, PotentialRouteControllerResultsClean, RouteControllerResponse } from '@gmp-types/routes';
import { RefreshTokensDatabaseInterface } from '@gmp-mongo/refresh-tokens';
import { UsersDatabaseInterface } from '@gmp-mongo/users';
import { AppError } from '@gmp-debug/errorhandler';

// export const isOfType = <T>(
//     varToBeChecked: any,
//     propertyToCheckFor: keyof T
//   ): varToBeChecked is T =>
//     (varToBeChecked as T)[propertyToCheckFor] !== undefined;

export class RouteController {
    private _databaseInterface: DatabaseInterface;

    /**
     * Constructor
     * @param databaseInterface 
     */
    constructor(databaseInterface: DatabaseInterface) {
        this._databaseInterface = databaseInterface;
    }

    /**
     * Private Functions
     */
    
    private _processResponse(routeControllerResults: PotentialRouteControllerResults | PotentialRouteControllerResultsClean, req: Request, res: Response, next: NextFunction): void {
        const routeControllerResponse: RouteControllerResponse = {
            info: {
                response: {
                    createdDate: new Date().toISOString()
                }
            },
            result: routeControllerResults
        };

        res.send(routeControllerResponse);
    }

    /**
     * Public Functions
     */

    /**
     * SECURITY
     */
    
    public async login(req: Request, res: Response, next: NextFunction): Promise<void> {
        if (this._databaseInterface instanceof UsersDatabaseInterface) {
            const result = await this._databaseInterface.login(req.body.username, req.body.password);
            this._processResponse(result, req, res, next);
        } else {
            errorHandler.catchError(new AppError('Incorrect Database Interface', 500), req, res, next);
        }
    }
    
    public async logout(req: Request, res: Response, next: NextFunction): Promise<void> {
        if (this._databaseInterface instanceof RefreshTokensDatabaseInterface) {
            const result = await this._databaseInterface.logout(req.body.token);
            this._processResponse(result, req, res, next);
        } else {
            errorHandler.catchError(new AppError('Incorrect Database Interface', 500), req, res, next);
        }
    }
    
    public async logoutAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        if (this._databaseInterface instanceof RefreshTokensDatabaseInterface) {
            const result = await this._databaseInterface.logout(req.currentUser.username);
            this._processResponse(result, req, res, next);
        } else {
            errorHandler.catchError(new AppError('Incorrect Database Interface', 500), req, res, next);
        }
    }
    
    public async refreshToken(req: Request, res: Response, next: NextFunction): Promise<void> {
        if (this._databaseInterface instanceof RefreshTokensDatabaseInterface) {
            const result = await this._databaseInterface.generateNewAuthToken(req.body.token);
            this._processResponse(result, req, res, next);
        } else {
            errorHandler.catchError(new AppError('Incorrect Database Interface', 500), req, res, next);
        }
    }

    /**
     * Get
     */

    public async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        const result = await this._databaseInterface.getAll();
        this._processResponse(result, req, res, next);
    }
    
    public async getById(req: Request, res: Response, next: NextFunction): Promise<void> {
        const result = await this._databaseInterface.getById(req.params.id);
        this._processResponse(result, req, res, next);
    }

    public async getByPrettyId(req: Request, res: Response, next: NextFunction): Promise<void> {
        const result = await this._databaseInterface.getByPrettyId(req.params.prettyId);
        this._processResponse(result, req, res, next);
    }

    /**
     * Insert
     */
    
    public async insert(req: Request, res: Response, next: NextFunction): Promise<void> {
        const result = await this._databaseInterface.insert(req.body);
        this._processResponse(result, req, res, next);
    }

    /**
     * Update
     */
    
    public async update(req: Request, res: Response, next: NextFunction): Promise<void> {
        const result = await this._databaseInterface.update(req.params.id, req.body);
        this._processResponse(result, req, res, next);
    }

    /**
     * Remove
     */
    
    public async remove(req: Request, res: Response, next: NextFunction): Promise<void> {
        const result = await this._databaseInterface.remove(req.params.id);
        this._processResponse(result, req, res, next);
    }
}