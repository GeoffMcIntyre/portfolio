import { logger, errorHandler } from '@gmp-debug';

import { NextFunction, Request, Response, Router } from 'express';

import expressRateLimiter from 'express-rate-limit';
import * as cache from '@gmp-services/cache';

import { jwtVerifyUser } from '@gmp-services/crypto';

import type { Language } from '@gmp-types/languages';
import type { AuthenticationOptions, CacheOptions, DelayOptions, RateLimitOptions, RouteItem, RouteOptions } from '@gmp-types/routes';

export class Route {
    // private _defaultCacheAgeInSeconds = 60 * 60 * 24;
    private _defaultCacheAgeSeconds = 10;

    private _defaultRateLimitwindowMS = 60000;
    private _defaultRateLimitMaxRequestCount = 20;

    private _mainLanguageId = 'eng';
    private _availableLanguages: Language[] = [
        {
            id: 'eng',
            name: 'English'
        },
        {
            id: 'kor',
            name: 'Korean'
        }
    ];

    private _router: Router;
    private _basePath: string;
    private _routeItems: RouteItem[] = [];

    private _authenticationController = (options: AuthenticationOptions | undefined) => {
        // No cache if age 0 or less
        if (options && typeof options.disabled != 'undefined' && options.disabled) {
            return (req: Request, res: Response, next: NextFunction) => {
                next();
            }
        }

        return (req: Request, res: Response, next: NextFunction) => {
            const authHeader = req.headers.authorization;

            if (authHeader) {
                try {
                    const token = authHeader.split(' ')[1];
                    const verifiedUser = jwtVerifyUser(token);
    
                    if (verifiedUser) {
                        if (options && typeof options.requiredPermissionLevels != 'undefined' && options.requiredPermissionLevels) {
                            
                            for (let i = 0; i < options.requiredPermissionLevels.length; i++){
                                if (verifiedUser.permissionLevel == options.requiredPermissionLevels[i]) {
                                    req.currentUser = verifiedUser;
                                    return next();
                                }
                            }
                        } else {
                            req.currentUser = verifiedUser;
                            return next();
                        }
                    }
                } catch(err) {
                    errorHandler.catchAuthenticationError(err, 'Bearer Token appears to be invalid', req, res, next);
                }
            }

            errorHandler.apiRejectAndExit('Bearer Token appears to be invalid', req, res, next);
        }
    };

    private _rateLimitReachedController = (req: Request, res: Response) => {
        errorHandler.catchError(new errorHandler.AppError('Rate limit reached'), req, res);
    };

    private _rateLimitController = (options: RateLimitOptions | undefined) => {
        const windowMS = options && options.windowMS ? options.windowMS : this._defaultRateLimitwindowMS;
        const maxRequests = options && options.maxRequests ? options.maxRequests : this._defaultRateLimitMaxRequestCount;

        const expressRateLimiterOptions: expressRateLimiter.Options = {
            windowMs: windowMS,
            max: maxRequests,
            handler: this._rateLimitReachedController, // called for each subsequent request once max is reached
        };

        return expressRateLimiter(expressRateLimiterOptions);
    };

    private _cacheController = (options: CacheOptions | undefined) => {
        // No cache if age 0 or less
        if (options && typeof options.ageSeconds != 'undefined' && options.ageSeconds <= 0) {
            return (req: Request, res: Response, next: NextFunction) => {
                next();
            }
        }

        const ageSeconds = options && options.ageSeconds ? options.ageSeconds : this._defaultCacheAgeSeconds;
        
        return cache.get().route(ageSeconds);
    };

    private _delayController = (options: DelayOptions | undefined) => {
        // No delay if disabled
        if (!options || !options.delay) {
            return (req: Request, res: Response, next: NextFunction) => {
                next();
            }
        }

        return (req: Request, res: Response, next: NextFunction) => {
            const between1and3seconds = 1000 + Math.floor(Math.random() * Math.floor(2000));
        
            new Promise(resolve => {
                setTimeout(() => {
                    logger.info(`Delay imposed: ${between1and3seconds} ms`);
                    resolve(next());
                }, between1and3seconds);
            });
        }
    };

    /**
     * Constructor
     * @param databaseInterface 
     */
    constructor(router: Router, basePath: string) {
        this._router = router;
        this._basePath = basePath;

        logger.info("Initialising Route: " + this._basePath);
    }

    private _buildPath(path?: string, language?: string): string {
        const languageChunk = language ? '/' + language : '';
        const pathChunk = path ? '/' + path : '';

        return languageChunk + '/' + this._basePath + pathChunk;
    }

    private async _addRoute(type: 'get' | 'post' | 'put' | 'delete', options: RouteOptions): Promise<void> {
        // Path
        const routePath: string = this._buildPath(options.path, options.language);

        // Router
        this._router[type](
            routePath,
            this._authenticationController(options.authenticationOptions),
            this._cacheController(options.cacheOptions),
            this._rateLimitController(options.rateLimitOptions),
            this._delayController(options.delayOptions),
            options.controller
        );

        // Log
        logger.info(`Added Route (${type}): ${routePath}`);
    }

    public addGet(options: RouteOptions): void {
        this._addRoute('get', options);
    }

    public addPost(options: RouteOptions): void {
        this._addRoute('post', options);
    }

    public addPut(options: RouteOptions): void {
        this._addRoute('put', options);
    }

    public addDelete(options: RouteOptions): void {
        this._addRoute('delete', options);
    }
}