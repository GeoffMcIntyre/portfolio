import { errorHandler, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';

import type { Db } from 'mongodb';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBInsertResultData, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from '@gmp-types/database';
import type { User } from '@gmp-types/users';

export class DatabaseInterface {
    private _db: Db;
    private _collectionProperties: CollectionProperties;

    /**
     * Constructor
     * @param collectionProperties 
     */
    constructor(db: Db, collectionProperties: CollectionProperties) {
        this._db = db;
        this._collectionProperties = collectionProperties;
    }

    /**
     * Private
     */

    private _logError(type: string, additionalInformation: string | null, err: Error): void {
        const additionalInformationString: string = additionalInformation ? ': ' + additionalInformation : '';
        errorHandler.catchDatabaseError(err, `${type} (${this.getCollectionProperties().collectionName}) failed${additionalInformationString}`);
    }

    private _logWarning(type: string, additionalInformation: string | null): void {
        const additionalInformationString: string = additionalInformation ? ': ' + additionalInformation : '';
        warningHandler.throwDatabaseWarning(`${type} (${this.getCollectionProperties().collectionName})${additionalInformationString}`);
    }

    private async _getMultiple(item: any): Promise<DBGetResult> {
        try {
            const result = await this._db.collection(this.getCollectionProperties().collectionName).find(item).toArray();

            if (result && result.length > 0) {
                return {
                    success: true,
                    message: 'Found',
                    data: result
                };
            }
        } catch (err) {
            this._logError('Get Multiple', JSON.stringify(item), err);
        }

        return {
            success: false,
            message: 'Not Found'
        };
    }

    private async _getOne(item: any): Promise<DBGetResult> {
        try {
            const result = await this._db.collection(this.getCollectionProperties().collectionName).findOne(item);

            if (result) {
                return {
                    success: true,
                    message: 'Found',
                    data: result
                };
            }
        } catch (err) {
            this._logError('Get One', JSON.stringify(item), err);
        }

        return {
            success: false,
            message: 'Not Found'
        };
    }
    
    private async _insertOne(item: any): Promise<DBInsertResult> {
        try {
            const { insertedCount, insertedId } = await this._db.collection(this.getCollectionProperties().collectionName).insertOne(item);

            if (insertedCount && insertedCount > 0) {
                return {
                    success: true,
                    message: 'Inserted',
                    data: {
                        id: insertedId,
                        count: insertedCount
                    }
                };
            }
        } catch (err) {
            this._logError('Insert One', JSON.stringify(item), err);
        }

        return {
            success: false,
            message: 'Not Inserted'
        };
    }
    
    private async _updateOne(id: string, item: any): Promise<DBUpdateResult> {
        try {
            const { ops } = await this._db.collection(this.getCollectionProperties().collectionName).update(
                { 
                    _id: new db.ObjectID(id)
                },
                {
                    $set: {
                        ...item,
                    }
                }
            );

            if (ops && ops.length > 0) {
                return {
                    success: true,
                    message: 'Updated'
                };
            }
        } catch (err) {
            this._logError('Update', id, err);
        }

        return {
            success: false,
            message: 'Not Updated'
        };
    }
    
    private async _removeOne(item: any): Promise<DBDeleteResult> {
        try {
            const { deletedCount } = await this._db.collection(this.getCollectionProperties().collectionName).deleteOne(item);

            if (deletedCount && deletedCount > 0) {
                return {
                    success: true,
                    message: 'Deleted'
                };
            }
        } catch (err) {
            this._logError('Remove One', JSON.stringify(item), err);
        }

        return {
            success: false,
            message: 'Not Deleted'
        };
    }

    /**
     * Protected
     */

    /**
     * Get
     */
    
    protected async _getBy(key: string, value: string | db.ObjectID): Promise<DBGetResult> {
        return await this._getOne(
            { 
                [key]: value
            }
        );
    }

     protected async _getAll(): Promise<DBGetResult> {
        return await this._getMultiple({});
    }

    protected async _getById(id: string): Promise<DBGetResult> {
        return await this._getBy('_id', new db.ObjectID(id));
    }

    protected async _getByName(name: string): Promise<DBGetResult> {
        return await this._getBy('name', name);
    }

    protected async _getByPrettyId(prettyId: string): Promise<DBGetResult> {
        return await this._getBy('prettyId', prettyId);
    }

    protected async _getByTitle(title: string): Promise<DBGetResult> {
        return await this._getBy('title', title);
    }

    protected async _getByUsername(username: string): Promise<DBGetResult> {
        return await this._getBy('username', username);
    }

    /**
    * Insert
    */

    protected async _insert(item: any, forceUpdate?: boolean): Promise<DBInsertResult> {
        return await this._insertOne(item);
    }

    protected async _insertMultiple(items: any[] | Record<string, unknown>[] | null, forceUpdate?: boolean): Promise<DBInsertResult> {
        const insertResultData: DBInsertResultData = {
            count: 0
        };

        if (items) {
            for (let i = 0; i < items.length; i++){
                const result = await this.insert(items[i], forceUpdate);

                if (result.success && result.data && result.data.id) {
                    insertResultData.count++;
                    insertResultData.ids?.push(result.data.id);
                }
            }
        }

        return {
            success: true,
            message: 'Inserted',
            data: insertResultData
        };
    }

    /**
    * Update
    */

    protected async _update(id: string, item: any): Promise<DBUpdateResult> {
        return await this._updateOne(id, item);
    }

    /**
    * Remove
    */
    
    protected async _removeBy(key: string, value: string | db.ObjectID): Promise<DBGetResult> {
        return await this._removeOne(
            { 
                [key]: value
            }
        );
    }

    protected async _remove(id: string): Promise<DBDeleteResult> {
        return await this._removeBy('id', new db.ObjectID(id));
    }

    /**
    * Clean Result
    */
    
    protected async _cleanGetResult(dirtyResult: DBGetResult): Promise<DBGetResultClean> {
        this._logWarning('Clean Get Result', 'result has not been cleaned ' + JSON.stringify(dirtyResult));
        return (dirtyResult as DBGetResultClean);
    }
    
    protected async _cleanInsertResult(dirtyResult: DBInsertResult): Promise<DBInsertResultClean> {
        //this._logWarning('Clean Insert Result', 'result has not been cleaned ' + JSON.stringify(dirtyResult));
        return (dirtyResult as DBInsertResultClean);
    }
    
    protected async _cleanUpdateResult(dirtyResult: DBUpdateResult): Promise<DBUpdateResultClean> {
        //this._logWarning('Clean Update Result', 'result has not been cleaned ' + JSON.stringify(dirtyResult));
        return (dirtyResult as DBUpdateResultClean);
    }
    
    protected async _cleanRemoveResult(dirtyResult: DBDeleteResult): Promise<DBDeleteResultClean> {
        //this._logWarning('Clean Remove Result', 'result has not been cleaned ' + JSON.stringify(dirtyResult));
        return (dirtyResult as DBDeleteResultClean);
    }

    /**
     * Public
     */

    /**
     * Collection Properties
     */

    public getDatabase(): Db {
        return this._db;
    }

    /**
     * Collection Properties
     */

    public getCollectionProperties(): CollectionProperties {
        return this._collectionProperties;
    }

    /**
     * Get
     */

    public async getAll(): Promise<DBGetResultClean> {
        return await this._cleanGetResult(await this._getMultiple({}));
    }

    public async getById(id: string): Promise<DBGetResultClean> {
        return await this._cleanGetResult(await this._getBy('_id', new db.ObjectID(id)));
    }

    public async getByName(name: string): Promise<DBGetResultClean> {
        return await this._cleanGetResult(await this._getBy('name', name));
    }

    public async getByPrettyId(prettyId: string): Promise<DBGetResultClean> {
        return await this._cleanGetResult(await this._getBy('prettyId', prettyId));
    }

    public async getByTitle(title: string): Promise<DBGetResultClean> {
        return await this._cleanGetResult(await this._getBy('title', title));
    }

    public async getByUsername(username: string): Promise<DBGetResultClean> {
        return await this._cleanGetResult(await this._getBy('username', username));
    }

    /**
    * Insert
    */

    public async insert(item: any, forceUpdate?: boolean): Promise<DBInsertResultClean> {
        return await this._cleanInsertResult(await this._insertOne(item));
    }

    public async insertMultiple(items: any[] | Record<string, unknown>[] | null, forceUpdate?: boolean): Promise<DBInsertResultClean> {
        const insertResultData: DBInsertResultData = {
            count: 0
        };

        if (items) {
            for (let i = 0; i < items.length; i++){
                const result = await this.insert(items[i], forceUpdate);

                if (result.success && result.data && result.data.id) {
                    insertResultData.count++;
                    insertResultData.ids?.push(result.data.id);
                }
            }
        }

        return this._cleanInsertResult({
            success: true,
            message: 'Inserted',
            data: insertResultData
        });
    }

    /**
    * Update
    */

    public async update(id: string, item: any): Promise<DBUpdateResultClean> {
        return await this._cleanUpdateResult(await this._updateOne(id, item));
    }

    /**
    * Remove
    */

    public async remove(id: string): Promise<DBDeleteResultClean> {
        return await this._cleanRemoveResult(await this._remove(id));
    }
}