export interface Ad {
	_id?: string;
	title: string;
	price: number;
}

export interface AdClean {
	title: string;
	price: number;
}

export interface DBGetAdResult {
    success: boolean;
    message: string;
	data: AdClean[] | AdClean;
}