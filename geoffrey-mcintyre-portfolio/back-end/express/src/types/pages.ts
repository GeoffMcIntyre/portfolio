// import { Url } from "url";

export type ContainerTypes =
	Container;

export type ComponentTypes =
	Container |
	OrderedList |
	Paragraph |
	UnorderedList;

export interface Page {
	_id?: string;
	containers?: ContainerTypes[];
}

export interface PageClean {
	_id?: string;
	containers?: ContainerTypes[];
}

export interface DBGetPageResult {
    success: boolean;
    message: string;
	data: PageClean[] | PageClean;
}

export interface Container {
	type: "container";
	containers?: ComponentTypes[];
}

export interface Image {
	type: "img";
	href: string;
}

export interface ListItem {
	text: string;
}

export interface OrderedList {
	type: "ol";
	items: ListItem[];
}

export interface Paragraph {
	type: "p";
	text: string;
}

export interface UnorderedList {
	type: "ul";
	items: ListItem[];
}
