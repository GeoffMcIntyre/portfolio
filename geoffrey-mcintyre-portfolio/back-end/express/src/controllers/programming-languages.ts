import { getDatabaseInterface as db } from '@gmp-mongo/programming-languages';

import { RouteController } from '@gmp-classes/routeController';

let routeController: RouteController

async function init(): Promise<void> {
    routeController = new RouteController(await db());
}

async function getRouteController(): Promise<RouteController> {
    if (!routeController) await init();

    return routeController;
}

export {
    getRouteController
};