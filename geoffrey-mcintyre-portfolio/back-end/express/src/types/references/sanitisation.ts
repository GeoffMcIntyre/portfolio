import { sanitiseString } from '@gmp-services/sanitisation';

import type { Reference } from '@gmp-types/references';

async function sanitiseReference(referenceDirty: Reference): Promise<Reference> {
    return {
        id: await sanitiseString(referenceDirty.id)
    };
}

async function sanitiseReferences(referencesDirty: Reference[]): Promise<Reference[]>{
    const referenceClean: Reference[] = [];

    for (let i = 0; i < referencesDirty.length; i++){
        referenceClean.push({
            id: await sanitiseString(referencesDirty[i].id)
        });
    }

    return referenceClean;
}

export {
    sanitiseReference,
    sanitiseReferences
}