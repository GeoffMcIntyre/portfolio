import { errorHandler, logger, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';
import { getDatabaseInterface as getProgrammingLanguagesDatabaseInterface } from './programming-languages';
import { getDatabaseInterface as getProjectPagesDatabaseInterface } from './project-pages';
import { getDatabaseInterface as getSoftwareDatabaseInterface } from './software';

import { DatabaseInterface } from '@gmp-classes/databaseInterface';
import { getProjectClean, getProjectDBEntryClean } from '@gmp-types/projects/interpreter';
import { sanitiseProject, sanitiseProjectDBEntry } from '@gmp-types/projects/sanitisation';

import type { Db } from 'mongodb';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean, DBInsertResultData } from '@gmp-types/database';
import type { Project, ProjectClean, ProjectDBEntry } from '@gmp-types/projects';
import type { Reference } from '@gmp-types/references';

import type { ProgrammingLanguageClean } from '@gmp-types/programming-languages';
import type { ProgrammingLanguagesDatabaseInterface } from './programming-languages';

import type { Page, PageClean } from '@gmp-types/pages';
import type { ProjectPagesDatabaseInterface } from './project-pages';

import type { SoftwareClean } from '@gmp-types/software';
import type { SoftwareDatabaseInterface } from './software';

const collectionProperties: CollectionProperties = {
    collectionName: 'projects',
    collectionSchema: {
        validator: {
            $jsonSchema: {
                bsonType: "object",
                required: [ "prettyId", "name" ],
                additionalProperties: false,
                properties: {
                    _id: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    },
                    prettyId: {
                        bsonType: "string",
                        description: "must be a string if the field exists"
                    },
                    name: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    dates: {
                        bsonType: "object",
                        required: [ "from" ],
                        additionalProperties: false,
                        properties: {
                            from: {
                                bsonType: "date",
                                description: "must be a date if the field exists"
                            },
                            to: {
                                bsonType: "date",
                                description: "must be a date if the field exists"
                            }
                        }
                    },
                    description: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    programmingLanguages: {
                        bsonType: "array",
                        minItems: 0,
                        uniqueItems: true,
                        additionalProperties: false,
                        items: {
                            bsonType: "object",
                            required: [ "id" ],
                            additionalProperties: false,
                            properties: {
                                id: {
                                    bsonType: "string",
                                    description: "must be a string if the field exists"
                                }
                            }
                        }
                    },
                    software: {
                        bsonType: "array",
                        minItems: 0,
                        uniqueItems: true,
                        additionalProperties: false,
                        items: {
                            bsonType: "object",
                            required: [ "id" ],
                            additionalProperties: false,
                            properties: {
                                id: {
                                    bsonType: "string",
                                    description: "must be a string if the field exists"
                                }
                            }
                        }
                    },
                    pageId: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    }
                }
            }
        }
    },
    collectionIndexes: [
        {
            "prettyId": 1
        }
    ],
    collectionIndexProperties: {
        unique: true,
        name: "query for projects"
    }
};

class ProjectsDatabaseInterface extends DatabaseInterface {
    constructor(db: Db, collectionProperties: CollectionProperties) {
        super(db, collectionProperties);
    }

    /**
     * Protected
     */

    /**
    * Clean
    */
    
    protected async _cleanGetResult(dirtyResult: DBGetResult): Promise<DBGetResultClean> {
        const cleanResult: DBGetResultClean = {
            success: dirtyResult.success,
            message: dirtyResult.message
        };

        if (Array.isArray(dirtyResult.data)) {
            const projects = (dirtyResult.data as Project[]);
            cleanResult.data = [];

            for (let i = 0; i < projects.length; i++){
                const projectClean = await getProjectClean(projects[i]);

                if (projectClean) {
                    cleanResult.data.push(projectClean);
                }
            }
        } else {
            const projectClean = await getProjectClean((dirtyResult.data as Project));

            if (projectClean) {
                cleanResult.data = projectClean;
            }
        }

        return cleanResult;
    }

    /**
     * Public
     */

    public async getAll(): Promise<DBGetResultClean> {
        return await super.getAll();
    }

    public async getById(id: string): Promise<DBGetResultClean> {
        return await super.getById(id);
    }
    
    public async getByName(name: string): Promise<DBGetResultClean> {
        return await super.getByName(name);
    }
    
    public async getByPrettyId(prettyId: string): Promise<DBGetResultClean> {
        const foundProject = await this._getByPrettyId(prettyId);

        if (foundProject && foundProject.data) {
            const cleanProject = await this._cleanEntry((foundProject.data as ProjectDBEntry));

            return {
                success: true,
                message: `Found`,
                data: cleanProject
            };
        }

        return {
            success: false,
            message: `Not Found`
        };
    }
    
    public async insert(project: Project): Promise<DBInsertResultClean> {
        const existing = await this._getByName(project.name);
        
        if (existing.success) {
            warningHandler.throwDatabaseWarning(`Project \`${project.name}\` already exists`);

            return {
                success: false,
                message: `Project \`${project.name}\` already exists`
            };
        }

        const dbEntry: ProjectDBEntry = await this._buildEntry(project);
        return await super.insert(dbEntry);
    }
    
    public async insertMultiple(projects: Project[]): Promise<DBInsertResultClean> {
        return await super.insertMultiple(projects);
    }
    
    public async update(id: string, project: Project): Promise<DBUpdateResultClean> {
        project = await sanitiseProject(project);

        const dbEntry: ProjectDBEntry = await this._buildEntry(project);
        return await super.update(id, dbEntry);
    }
    
    public async remove(id: string): Promise<DBDeleteResultClean> {
        return await super.remove(id);
    } 
    
    private async _cleanEntry(dbEntry: ProjectDBEntry): Promise<ProjectClean> {
        const populatedProject: ProjectClean = {
            prettyId: dbEntry.prettyId,
            name: dbEntry.name,
            dates: {
                from: new Date(dbEntry.dates.from)
            },
            description: dbEntry.description,
            programmingLanguages: [],
            software: []
        };
    
        if (dbEntry.dates.to) {
            populatedProject.dates.to = new Date(dbEntry.dates.to);
        }
    
        for (let i = 0; i < dbEntry.programmingLanguages.length; i++){
            const projectProgrammingLanguage = dbEntry.programmingLanguages[i];

            const foundProgrammingLanguage = await getProgrammingLanguagesDatabaseInterface().then(async (programmingLanguagesDatabaseInterface: ProgrammingLanguagesDatabaseInterface) => {
                return await programmingLanguagesDatabaseInterface.getByName(projectProgrammingLanguage.id);
            });
    
            if (foundProgrammingLanguage && foundProgrammingLanguage.data && (foundProgrammingLanguage.data as ProgrammingLanguageClean)) {
                populatedProject.programmingLanguages.push((foundProgrammingLanguage.data as ProgrammingLanguageClean));
            }
        }
    
        for (let i = 0; i < dbEntry.software.length; i++){
            const projectSoftware = dbEntry.software[i];

            const foundSoftware = await getSoftwareDatabaseInterface().then(async (softwareDatabaseInterface: SoftwareDatabaseInterface) => {
                return await softwareDatabaseInterface.getByName(projectSoftware.id);
            });
    
            if (foundSoftware && foundSoftware.data && (foundSoftware.data as SoftwareClean)) {
                populatedProject.software.push((foundSoftware.data as SoftwareClean));
            }
        }
    
        if (dbEntry.pageId) {
            const foundProjectPage = await getProjectPagesDatabaseInterface().then(async (projectPagesDatabaseInterface: ProjectPagesDatabaseInterface) => {
                if (dbEntry.pageId) {
                    return await projectPagesDatabaseInterface.getById(dbEntry.pageId);
                }
            });
            
            if (foundProjectPage && foundProjectPage.data) {
                populatedProject.page = (foundProjectPage.data as PageClean);
            }
        }
    
        return populatedProject;
    }
    
    private async _buildEntry(project: Project): Promise<ProjectDBEntry> {
        const dbEntry: ProjectDBEntry = {
            prettyId: project.prettyId,
            name: project.name,
            dates: {
                from: new Date(project.dates.from)
            },
            description: project.description,
            programmingLanguages: [],
            software: []
        };
    
        if (project.dates.to) {
            dbEntry.dates.to = new Date(project.dates.to);
        }
    
        for (let i = 0; i < project.programmingLanguages.length; i++){
            let id;
            const projectProgrammingLanguage = project.programmingLanguages[i];
            const foundProgrammingLanguage = await getProgrammingLanguagesDatabaseInterface().then(async (programmingLanguagesDatabaseInterface: ProgrammingLanguagesDatabaseInterface) => {
                return await programmingLanguagesDatabaseInterface.getByName(projectProgrammingLanguage.name);
            });

            if (foundProgrammingLanguage.success) {
                id = projectProgrammingLanguage.name;
            } else {
                const newProgrammingLanguage = await getProgrammingLanguagesDatabaseInterface().then(async (programmingLanguagesDatabaseInterface: ProgrammingLanguagesDatabaseInterface) => {
                    return await programmingLanguagesDatabaseInterface.insert(projectProgrammingLanguage, false);
                });

                if (newProgrammingLanguage && newProgrammingLanguage.data) {
                    id = projectProgrammingLanguage.name;
                }
            }
    
            if (id) {
                const referenceEntry: Reference = {
                    id: id
                };
        
                dbEntry.programmingLanguages.push(referenceEntry);
            }
        }
    
        for (let i = 0; i < project.software.length; i++){
            let id;
            const projectSoftware = project.software[i];
            const foundSoftware = await getSoftwareDatabaseInterface().then(async (softwareDatabaseInterface: SoftwareDatabaseInterface) => {
                return await softwareDatabaseInterface.getByName(projectSoftware.name);
            });

            if (foundSoftware.success) {
                id = projectSoftware.name;
            } else {
                const newSoftware: DBInsertResult = await getSoftwareDatabaseInterface().then(async (softwareDatabaseInterface: SoftwareDatabaseInterface) => {
                    return await softwareDatabaseInterface.insert(projectSoftware, false);
                });

                if (newSoftware && newSoftware.data) {
                    id = projectSoftware.name;
                }
            }
            
            if (id) {
                const referenceEntry: Reference = {
                    id: id
                };
    
                dbEntry.software.push(referenceEntry);
            }
        }
    
        let projectPageId;
    
        if (project._id) {
            const foundProjectPage = await getProjectPagesDatabaseInterface().then(async (projectPagesDatabaseInterface: ProjectPagesDatabaseInterface) => {
                if (project._id) {
                    return await projectPagesDatabaseInterface.getById(project._id);
                }
            });
    
            if (foundProjectPage && foundProjectPage.success) {
                projectPageId = (foundProjectPage.data as Page)._id;
            }
        } else if (project.page) {
            const newProjectPage = await getProjectPagesDatabaseInterface().then(async (projectPagesDatabaseInterface: ProjectPagesDatabaseInterface) => {
                if (project.page) {
                    return await projectPagesDatabaseInterface.insert(project.page);
                }
            });

            if (newProjectPage && newProjectPage.success) {
                projectPageId = (newProjectPage.data as DBInsertResultData).id;
            }
        }
    
        if (projectPageId) {
            dbEntry.pageId = projectPageId;
        }
        
        return dbEntry;
    }
}

let databaseInterface: ProjectsDatabaseInterface;

async function init(): Promise<void> {
    databaseInterface = new ProjectsDatabaseInterface(await db.getSiteComponentsDb(), collectionProperties);
}

async function getDatabaseInterface(): Promise<ProjectsDatabaseInterface> {
    if (!databaseInterface) await init();

    return databaseInterface;
}

export {
    ProjectsDatabaseInterface,
    getDatabaseInterface
};