#!/bin/bash

set -e

# dbUser is the userName used from applicatoin code to interact with databases and dbPwd is the password for this user.
# MONGO_INITDB_ROOT_USERNAME & MONGO_INITDB_ROOT_PASSWORD is the config for db admin.
# admin user is expected to be already created when this script executes. We use it here to authenticate as admin to create
# dbUser and databases.

# Admin User
MONGODB_ADMIN_USER=${MONGODB_ADMIN_USER:-"admin"}
MONGODB_ADMIN_PASS=${MONGODB_ADMIN_PASS:-"4dmInP4ssw0rd"}

# Application Database User
MONGODB_APPLICATION_DATABASE=${MONGODB_APPLICATION_DATABASE:-"admin"}
MONGODB_APPLICATION_USER=${MONGODB_APPLICATION_USER:-"restapiuser"}
MONGODB_APPLICATION_PASS=${MONGODB_APPLICATION_PASS:-"r3sT4pIp4ssw0rd"}

mongod --auth &

echo ">>>>>>> trying to create database and users"
if [ -n "${MONGODB_ADMIN_USER:-}" ] && [ -n "${MONGODB_ADMIN_PASS:-}" ] && [ -n "${MONGODB_APPLICATION_USER:-}" ] && [ -n "${MONGODB_APPLICATION_PASS:-}" ]; then
mongo -u $MONGODB_ADMIN_USER -p $MONGODB_ADMIN_PASS<<EOF
db=db.getSiblingDB('admin');
db=db.getSiblingDB('$MONGODB_APPLICATION_DATABASE');
use admin;
db.createUser({
  user:  '$MONGODB_APPLICATION_USER',
  pwd: '$MONGODB_APPLICATION_PASS',
  roles: [{
    role: 'root',
    db: 'admin'
  }]
});
use $MONGODB_APPLICATION_DATABASE;
db.createUser({
  user:  '$MONGODB_APPLICATION_USER',
  pwd: '$MONGODB_APPLICATION_PASS',
  roles: [{
    role: 'dbOwner',
    db: '$MONGODB_APPLICATION_DATABASE'
  }]
});
EOF
else
    echo "MONGODB_ADMIN_USER, MONGODB_ADMIN_PASS, MONGODB_APPLICATION_DATABASE, MONGODB_APPLICATION_USER and MONGODB_APPLICATION_PASS must be provided. Some of these are missing, hence exiting database and user creatioin"
    exit 403
fi