import type { ProgrammingLanguage, ProgrammingLanguageClean } from '@gmp-types/programming-languages';

async function getProgrammingLanguageClean(programmingLanguageDirty: ProgrammingLanguage): Promise<ProgrammingLanguageClean | undefined>{
    if (programmingLanguageDirty) {
        return {
            name: programmingLanguageDirty.name,
            description: programmingLanguageDirty.description
        };
    }
}

export {
    getProgrammingLanguageClean
}