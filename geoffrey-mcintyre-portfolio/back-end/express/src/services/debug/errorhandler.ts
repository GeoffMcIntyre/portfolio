import * as logger from '@gmp-debug/logger';

import { Request, Response, NextFunction } from 'express';

class AppError extends Error {
    statusCode?: number;
    status: string;
    isOperational: boolean;

    constructor(message: string, statusCode?: number) {
        super(message);

        if (statusCode) {
            this.statusCode = statusCode;
        }
        
        this.status = `${statusCode}`.startsWith('4') ? 'failure' : 'error';
        this.isOperational = true;
        
        Error.captureStackTrace(this, this.constructor);
    }
}

const logErrorDev = (err: AppError) => {
    err.statusCode = err.statusCode || 500;

    logger.error('ERROR 💥: ', err);
};

const sendErrorDev = (err: AppError, res: Response) => {
    err.statusCode = err.statusCode || 500;

    logErrorDev(err);
    
    res.status(err.statusCode).json({
        status: err.status,
        error: err,
        message: err.message,
        stack: err.stack
    });
};

const logErrorProd = (err: AppError) => {
    err.statusCode = err.statusCode || 500;
    
    // Operational, trusted error: send message to client
    if (err.isOperational) {
        logger.error('OPERATIONAL ERROR 💥: ', err);
    // Programming or other unknown error
    } else {
        logger.error('ERROR 💥: ', err);
    }
};

const sendErrorProd = (err: AppError, res: Response) => {
    err.statusCode = err.statusCode || 500;

    logErrorProd(err);
    
    // Operational, trusted error: send message to client
    if (err.isOperational) {
        res.status(err.statusCode).json({
            status: err.status,
            message: err.message
        });
    // Programming or other unknown error
    } else {
        res.status(500).json({
            status: 'error',
            message: 'Something went very wrong!'
        });
    }
};

async function catchError(err: AppError): Promise<void>;
async function catchError(err: AppError, req?: Request, res?: Response): Promise<void>;
async function catchError(err: AppError, req?: Request, res?: Response, next?: NextFunction): Promise<void>;
async function catchError(err: AppError, req?: Request, res?: Response, next?: NextFunction): Promise<void> {
    err.status = err.status || 'error';

    if (res) {
        err.statusCode = err.statusCode || 500;

        logger.error('(' + err.statusCode + ') ' + err.message);
  
        if (process.env.NODE_ENV === 'development') {
            sendErrorDev(err, res);
        } else if (process.env.NODE_ENV === 'production') {
            sendErrorProd(err, res);
        }
    } else {
        if (process.env.NODE_ENV === 'development') {
            logErrorDev(err);
        } else if (process.env.NODE_ENV === 'production') {
            logErrorProd(err);
        }
    }
}

async function apiDataRetrievalError(req: Request): Promise<AppError> {
    return new AppError(`Could not retrieve requested data from: ${req.originalUrl}`);
}

async function apiRejectAndExit(message: string, req?: Request, res?: Response, next?: NextFunction): Promise<void> {
    catchAuthenticationError(new AppError(`API Request Rejected`), message, req, res, next);
}

async function catchAuthenticationError(err: Error, message?: string, req?: Request, res?: Response, next?: NextFunction): Promise<void> {
    const additionalMessage = message ? " | More Information: " + message : "";

    catchError(new AppError(`Authentication ${err}${additionalMessage}`), req, res, next);
}

async function catchBasicError(err: Error, message?: string, req?: Request, res?: Response, next?: NextFunction): Promise<void> {
    const additionalMessage = message ? " | More Information: " + message : "";

    catchError(new AppError(`${err}${additionalMessage}`), req, res, next);
}

async function catchCacheError(err: Error, message?: string, req?: Request, res?: Response, next?: NextFunction): Promise<void> {
    const additionalMessage = message ? " | More Information: " + message : "";

    catchError(new AppError(`Cache ${err}${additionalMessage}`), req, res, next);
}

async function catchCryptoError(err: Error, message?: string, req?: Request, res?: Response, next?: NextFunction): Promise<void> {
    const additionalMessage = message ? " | More Information: " + message : "";

    catchError(new AppError(`Crypto ${err}${additionalMessage}`), req, res, next);
}

async function catchDatabaseError(err: Error, message?: string, req?: Request, res?: Response, next?: NextFunction): Promise<void> {
    const additionalMessage = message ? " | More Information: " + message : "";

    catchError(new AppError(`Database ${err}${additionalMessage}`), req, res, next);
}

async function tryCatch(func: () => any): Promise<any> {
    try {
        return func();
    } catch (err) {
        catchError(new AppError(`Caught: ${err}`));
    }

    return null;
}

export {
    AppError,
    apiRejectAndExit,
    apiDataRetrievalError,
    catchError,
    catchAuthenticationError,
    catchBasicError,
    catchCacheError,
    catchCryptoError,
    catchDatabaseError,
    tryCatch
};