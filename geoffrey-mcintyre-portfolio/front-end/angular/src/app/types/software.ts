export interface Software {
	_id: string;
	name: string;
	description: string;
}