import { Component, OnInit } from '@angular/core';
import { ComponentFunctionsService } from '@gmp-services/component-functions.service';
import { AdsService } from './ads.service';

@Component({
    selector: 'app-ads',
    templateUrl: './ads.component.html',
    styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {
	public title = 'Geoffs Portfolio - Ads';

	private ads:any[] = [];
  
	public constructor(
		private componentFunctionsService: ComponentFunctionsService,
		private adsService: AdsService
	) {
		componentFunctionsService.setTitle(this.title);
	}

	async ngOnInit(): Promise<void> {
		const data: any[] = await this.adsService.getAds();

		this.ads = data;

		console.log("Ads", this.ads);
	}

	// showConfig() {

	// }

}
