import { Component } from '@angular/core';
import { ComponentFunctionsService } from '@gmp-services/component-functions.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	public title = 'Geoffs Portfolio';
  
	public constructor(private componentFunctionsService: ComponentFunctionsService) { 
		componentFunctionsService.setTitle(this.title);
	}
}
