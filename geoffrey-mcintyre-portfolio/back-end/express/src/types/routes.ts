import { NextFunction, Request, Response } from 'express';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from './database';

export type PotentialRouteControllerResults =
    DBGetResult |
	DBInsertResult |
	DBUpdateResult |
	DBDeleteResult;

export type PotentialRouteControllerResultsClean =
    DBGetResultClean |
    DBInsertResultClean |
    DBUpdateResultClean |
    DBDeleteResultClean;

export interface RouteControllerResponse {
    info: RouteControllerInfo;
    result: PotentialRouteControllerResults | PotentialRouteControllerResultsClean;
}

export interface RouteControllerInfo {
    response: RouteControllerResponseInfo;
}

export interface RouteControllerResponseInfo {
    createdDate?: string;
}

export interface RouteItem {
    path: string;
}

export interface RouteOptions {
    path?: string;
    language?: string;
    authenticationOptions?: AuthenticationOptions;
    cacheOptions?: CacheOptions;
    rateLimitOptions?: RateLimitOptions;
    delayOptions?: DelayOptions;
    controller(req: Request, res: Response, next: NextFunction): void;
}

export interface RateLimitOptions {
    windowMS?: number;
    maxRequests?: number;
}

export interface CacheOptions {
    ageSeconds?: number;
}

export interface DelayOptions {
    delay?: boolean;
}

export interface AuthenticationOptions {
    disabled: boolean;
    requiredPermissionLevels?: number[];
}