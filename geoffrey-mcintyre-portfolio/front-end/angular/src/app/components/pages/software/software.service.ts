import { Injectable } from '@angular/core';
import { DataService } from '@gmp-services/data.service';
import { Observable, of } from 'rxjs';

@Injectable()
export class SoftwareService {

	constructor(private dataService: DataService) { 
		
	}
	
	async getSoftware(): Promise<any[]>{
		return await this.dataService.sendGetRequest('software');
	}
}