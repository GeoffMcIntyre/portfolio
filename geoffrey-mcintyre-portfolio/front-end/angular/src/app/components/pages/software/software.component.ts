import { Component, OnInit } from '@angular/core';
import { ComponentFunctionsService } from '@gmp-services/component-functions.service';
import { SoftwareService } from './software.service';

@Component({
  selector: 'app-software',
  templateUrl: './software.component.html',
  styleUrls: ['./software.component.scss']
})
export class SoftwareComponent implements OnInit {
	public title = 'Geoffs Portfolio - Software';

	private software:any[] = [];
  
	public constructor(
		private componentFunctionsService: ComponentFunctionsService,
		private softwareService: SoftwareService
	) {
		componentFunctionsService.setTitle(this.title);
	}

	async ngOnInit(): Promise<void> {
		const data: any[] = await this.softwareService.getSoftware();

		this.software = data;

		console.log("Software", this.software);
	}

	// showConfig() {

	// }

}
