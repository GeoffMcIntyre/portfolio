import { NextFunction, Request, Response, Router } from 'express';

import { getRouteController } from '@gmp-controllers/refresh-tokens';

import { Route } from '@gmp-classes/route';

async function init(router: Router): Promise<void> {
    const route = new Route(router, 'tokens');
    const routeController = await getRouteController();
    
    route.addPost({
        path: 'jwtRefresh',
        authenticationOptions: {
            disabled: true
        },
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.refreshToken(req, res, next)
    });
}

export {
    init
};