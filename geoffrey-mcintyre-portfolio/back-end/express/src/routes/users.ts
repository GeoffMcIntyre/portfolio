import { NextFunction, Request, Response, Router } from 'express';

import { getRouteController } from '@gmp-controllers/users';

import { Route } from '@gmp-classes/route';

async function init(router: Router): Promise<void> {
    const route = new Route(router, 'users');
    const routeController = await getRouteController();

    route.addGet({
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.getAll(req, res, next)
    });

    route.addGet({
        path: ':id',
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.getById(req, res, next)
    });

    route.addPost({
        authenticationOptions: {
            disabled: true
        },
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.insert(req, res, next)
    });

    route.addPut({
        path: ':id',
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.update(req, res, next)
    });

    route.addDelete({
        path: ':id',
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.remove(req, res, next)
    });
}

export {
    init
};