import type { Page, PageClean } from '@gmp-types/pages';

async function getPageClean(pageDirty: Page): Promise<PageClean | undefined>{
    if (pageDirty) {
        return {
            _id: pageDirty._id,
            containers: pageDirty.containers
        };
    }
}

export {
    getPageClean
}