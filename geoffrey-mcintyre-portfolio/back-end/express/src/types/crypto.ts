export interface JwtTokens {
    token: string;
    refreshToken: string;
}

export interface JwtToken {
    token: string;
}