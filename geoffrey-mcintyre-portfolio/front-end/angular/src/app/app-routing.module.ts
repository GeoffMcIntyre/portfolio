import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdsComponent } from '@gmp-c-pages/ads/ads.component';
import { ProgrammingLanguagesComponent } from '@gmp-c-pages/programming-languages/programming-languages.component';
import { ProjectComponent } from '@gmp-c-pages/project/project.component';
import { ProjectsComponent } from '@gmp-c-pages/projects/projects.component';
import { SoftwareComponent } from '@gmp-c-pages/software/software.component';

const routes: Routes = [
	{
		path: 'ads',
		component: AdsComponent
	},
	{
		path: 'programming-languages',
		component: ProgrammingLanguagesComponent
	},
	{
		path: 'projects',
		component: ProjectsComponent
	},
	{
		path: 'projects/:projectId',
		component: ProjectComponent
	},
	{
		path: 'software',
		component: SoftwareComponent
	}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
