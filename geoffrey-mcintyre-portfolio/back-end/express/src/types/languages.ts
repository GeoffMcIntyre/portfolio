export interface Language {
    id: string;
    name: string;
}

export interface LanguageClean {
	title: string;
	price: number;
}

export interface DBGetLanguageResult {
    success: boolean;
    message: string;
	data: LanguageClean[] | LanguageClean;
}