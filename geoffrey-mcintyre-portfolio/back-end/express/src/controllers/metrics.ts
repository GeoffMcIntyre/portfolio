import { getPromClientRegister } from '@gmp-services/metrics';

import { NextFunction, Request, Response } from 'express';

async function getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    const register = await getPromClientRegister();
    
    const sendSuccesss = (output:string) => {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end(output);
    };

    const metricsResponse: Promise<string> = register.metrics();
    
    metricsResponse
        .then(output => sendSuccesss(output))
        .catch(err => next(err));
}

export {
    getAll
};