import { errorHandler, logger, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';
import { DatabaseInterface } from '@gmp-classes/databaseInterface';
import { getSoftwareClean } from '@gmp-types/software/interpreter';
import { sanitiseSoftware } from '@gmp-types/software/sanitisation';

import type { Db } from 'mongodb';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from '@gmp-types/database';
import type { Software, SoftwareClean } from '@gmp-types/software';

const collectionProperties: CollectionProperties = {
    collectionName: 'software',
    collectionSchema: {
        validator: {
            $jsonSchema: {
                bsonType: "object",
                required: [ "name" ],
                additionalProperties: false,
                properties: {
                    _id: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    },
                    name: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    description: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    }
                }
            }
        }
    },
    collectionIndexes: [
        {
            "name": 1
        }
    ],
    collectionIndexProperties: {
        unique: true,
        name: "query for software"
    }
};

class SoftwareDatabaseInterface extends DatabaseInterface {
    constructor(db: Db, collectionProperties: CollectionProperties) {
        super(db, collectionProperties);
    }

    /**
     * Protected
     */

    /**
    * Clean
    */
    
    protected async _cleanGetResult(dirtyResult: DBGetResult): Promise<DBGetResultClean> {
        const cleanResult: DBGetResultClean = {
            success: dirtyResult.success,
            message: dirtyResult.message
        };

        if (Array.isArray(dirtyResult.data)) {
            const softwares = (dirtyResult.data as Software[]);
            cleanResult.data = [];

            for (let i = 0; i < softwares.length; i++){
                const softwareClean = await getSoftwareClean(softwares[i]);

                if (softwareClean) {
                    cleanResult.data.push(softwareClean);
                }
            }
        } else {
            const softwareClean = await getSoftwareClean((dirtyResult.data as Software));

            if (softwareClean) {
                cleanResult.data = softwareClean;
            }
        }

        return cleanResult;
    }

    /**
     * Public
     */

    public async getAll(): Promise<DBGetResultClean> {
        return await super.getAll();
    }

    public async getById(id: string): Promise<DBGetResultClean> {
        return await super.getById(id);
    }
    
    public async getByName(name: string): Promise<DBGetResultClean> {
        return await super.getByName(name);
    }
    
    public async insert(software: Software, forceUpdate?: boolean): Promise<DBInsertResultClean | DBUpdateResultClean> {
        software = await sanitiseSoftware(software);

        const existing = await this._getByName(software.name);
        
        if ((existing.data as Software)) {
            const id = (existing.data as Software)._id;

            if (forceUpdate && id) {
                return await this.update(id, software);
            }

            warningHandler.throwDatabaseWarning(`Software \`${software.name}\` already exists`);

            return {
                success: false,
                message: `Software \`${software.name}\` already exists`
            };
        }

        return await super.insert(software);
    }
    
    public async insertMultiple(softwares: Software[], forceUpdate?: boolean): Promise<DBInsertResultClean> {
        return await super.insertMultiple(softwares, forceUpdate);
    }
    
    public async update(id: string, software: Software): Promise<DBUpdateResultClean> {
        software = await sanitiseSoftware(software);

        return await super.update(id, software);
    }
    
    public async remove(id: string): Promise<DBDeleteResultClean> {
        return await super.remove(id);
    }
}

let databaseInterface: SoftwareDatabaseInterface;

async function init(): Promise<void> {
    databaseInterface = new SoftwareDatabaseInterface(await db.getSiteComponentsDb(), collectionProperties);
}

async function getDatabaseInterface(): Promise<SoftwareDatabaseInterface> {
    if (!databaseInterface) await init();

    return databaseInterface;
}

export {
    SoftwareDatabaseInterface,
    getDatabaseInterface
};