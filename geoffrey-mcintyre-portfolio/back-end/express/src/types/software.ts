export interface Software {
	_id?: string;
	name: string;
	description?: string;
}

export interface SoftwareClean {
	name: string;
	description?: string;
}

export interface DBGetSoftwareResult {
    success: boolean;
    message: string;
	data: SoftwareClean[] | SoftwareClean;
}