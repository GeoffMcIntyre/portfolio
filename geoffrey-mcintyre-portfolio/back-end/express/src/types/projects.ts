import type { Page, PageClean } from './pages';
import type { ProgrammingLanguage, ProgrammingLanguageClean } from './programming-languages';
import type { Reference } from './references';
import type { Software, SoftwareClean } from './software';

export interface Project {
	_id?: string;
	prettyId: string;
	name: string;
	dates: ProjectDates;
	description: string;
	programmingLanguages: ProgrammingLanguage[];
	software: Software[];
	page?: Page;
}

export interface ProjectClean {
	prettyId: string;
	name: string;
	dates: ProjectDates;
	description: string;
	programmingLanguages: ProgrammingLanguageClean[];
	software: SoftwareClean[];
	page?: PageClean;
}

export interface DBGetProjectResult {
    success: boolean;
    message: string;
	data: ProjectClean[] | ProjectClean;
}

export interface ProjectDBEntry {
	_id?: string;
	prettyId: string;
	name: string;
	dates: ProjectDates;
	description: string;
	programmingLanguages: Reference[];
	software: Reference[];
	pageId?: string;
}

export interface ProjectDates {
	from: Date;
	to?: Date;
}
