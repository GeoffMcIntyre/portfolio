import * as secrets from '@gmp-services/secrets';

import type { ConfigEnvironment } from '@gmp-types/config';

function getProduction(): ConfigEnvironment {
    return {
        httpPort: process.env.EXPRESS_API_PORT || -1,
        httpAddress: process.env.EXPRESS_API_HOST || 'localhost',
        envName: 'production',
        log: {
            level: process.env.LOG_LEVEL || 0
        },
        databases: {
            mongo: {
                siteComponents: {
                    url: secrets.read('MONGODB_SITE_COMPONENTS_URL_FILE') || process.env.MONGODB_SITE_COMPONENTS_URL,
                    name: 'gmp-site-components'
                },
                userAccounts: {
                    url: secrets.read('MONGODB_USER_ACCOUNTS_URL_FILE') || process.env.MONGODB_USER_ACCOUNTS_URL,
                    name: 'gmp-user-accounts'
                },
                userGenerated: {
                    url: secrets.read('MONGODB_USER_GENERATED_URL_FILE') || process.env.MONGODB_USER_GENERATED_URL,
                    name: 'gmp-user-generated'
                }
            },
            redis: {
                cache: {
                    url: process.env.REDIS_URL || 'redis-database',
                    port: process.env.REDIS_PORT || -1
                }
            }
        },
        jwt: {
            secret: secrets.read('JWT_SECRET_FILE') || process.env.JWT_SECRET,
            refreshSecret: secrets.read('JWT_REFRESH_SECRET_FILE') || process.env.JWT_REFRESH_SECRET
        }
    };
}

function get(): ConfigEnvironment {
    const currentEnvironment = typeof process.env.NODE_ENV === 'string' ? process.env.NODE_ENV.toLowerCase() : '';

    switch (currentEnvironment) {
        case 'production':
        default:
            return getProduction();
    }
}

export {
    get
}