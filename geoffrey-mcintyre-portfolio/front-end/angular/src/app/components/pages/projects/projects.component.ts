import { Component, OnInit } from '@angular/core';
import { ComponentFunctionsService } from '@gmp-services/component-functions.service';
import { ProjectsService } from './projects.service';

import type { Project } from '@gmp-types/projects';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
	public title = 'Geoffs Portfolio - Projects';

	private projects:Project[] = [];
  
	public constructor(
		private componentFunctionsService: ComponentFunctionsService,
		private projectsService: ProjectsService
	) {
		componentFunctionsService.setTitle(this.title);
	}

	async ngOnInit(): Promise<void> {
		const data: any[] = await this.projectsService.getProjects();

		this.projects = data;

		console.log("Projects", this.projects);
	}

	// showConfig() {

	// }

}
