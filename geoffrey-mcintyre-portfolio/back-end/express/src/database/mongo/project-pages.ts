import { errorHandler, logger, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';
import { DatabaseInterface } from '@gmp-classes/databaseInterface';
import { getPageClean } from '@gmp-types/pages/interpreter';
import { sanitisePage } from '@gmp-types/pages/sanitisation';

import type { Db } from 'mongodb';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from '@gmp-types/database';
import type { Page, PageClean } from '@gmp-types/pages';

const collectionProperties: CollectionProperties = {
    collectionName: 'project-pages',
    collectionSchema: {
        validator: {
            $jsonSchema: {
                bsonType: "object",
                required: [ "containers" ],
                additionalProperties: false,
                properties: {
                    _id: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    },
                    containers: {
                        bsonType: "array",
                        minItems: 0,
                        uniqueItems: true,
                        additionalProperties: true,
                        items: {
                            bsonType: "object",
                            required: [ "type" ],
                            additionalProperties: true,
                            properties: {
                                type: {
                                    bsonType: "string",
                                    description: "must be a objectId if the field exists"
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    collectionIndexes: null,
    collectionIndexProperties: null
};

class ProjectPagesDatabaseInterface extends DatabaseInterface {
    constructor(db: Db, collectionProperties: CollectionProperties) {
        super(db, collectionProperties);
    }

    /**
     * Protected
     */

    /**
    * Clean
    */
    
    protected async _cleanGetResult(dirtyResult: DBGetResult): Promise<DBGetResultClean> {
        const cleanResult: DBGetResultClean = {
            success: dirtyResult.success,
            message: dirtyResult.message
        };

        if (Array.isArray(dirtyResult.data)) {
            const pages = (dirtyResult.data as Page[]);
            cleanResult.data = [];

            for (let i = 0; i < pages.length; i++){
                const pageClean = await getPageClean(pages[i]);

                if (pageClean) {
                    cleanResult.data.push(pageClean);
                }
            }
        } else {
            const pageClean = await getPageClean((dirtyResult.data as Page));

            if (pageClean) {
                cleanResult.data = pageClean;
            }
        }

        return cleanResult;
    }

    /**
     * Public
     */

    public async getAll(): Promise<DBGetResultClean> {
        return await super.getAll();
    }

    public async getById(id: string): Promise<DBGetResultClean> {
        return await super.getById(id);
    }
    
    public async getByName(name: string): Promise<DBGetResultClean> {
        return await super.getByName(name);
    }
    
    public async insert(page: Page): Promise<DBInsertResultClean> {
        page = await sanitisePage(page);
        
        if (page._id) {
            const existing = await this._getById(page._id);
        
            if (existing.success) {
                warningHandler.throwDatabaseWarning(`Project Page \`${page._id}\` already exists`);

                return {
                    success: false,
                    message: `Project Page \`${page._id}\` already exists`
                };
            }
        }

        return await super.insert(page);
    }
    
    public async insertMultiple(pages: Page[]): Promise<DBInsertResultClean> {
        return await super.insertMultiple(pages);
    }
    
    public async update(id: string, page: Page): Promise<DBUpdateResultClean> {
        page = await sanitisePage(page);

        return await super.update(id, page);
    }
    
    public async remove(id: string): Promise<DBDeleteResultClean> {
        return await super.remove(id);
    }
}

let databaseInterface: ProjectPagesDatabaseInterface;

async function init(): Promise<void> {
    databaseInterface = new ProjectPagesDatabaseInterface(await db.getSiteComponentsDb(), collectionProperties);
}

async function getDatabaseInterface(): Promise<ProjectPagesDatabaseInterface> {
    if (!databaseInterface) await init();

    return databaseInterface;
}

export {
    ProjectPagesDatabaseInterface,
    getDatabaseInterface
};