import { sanitiseNumber, sanitiseString } from '@gmp-services/sanitisation';

import type { Ad } from '@gmp-types/ads';

async function sanitiseAd(adDirty: Ad): Promise<Ad>{
    return {
        title: await sanitiseString(adDirty.title),
        price: await sanitiseNumber(adDirty.price)
    };
}

export {
    sanitiseAd
}