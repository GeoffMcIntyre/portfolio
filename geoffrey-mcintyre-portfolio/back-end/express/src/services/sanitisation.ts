import stripAnsi from 'strip-ansi';
import escapeHtml from 'escape-html';
import cssesc from 'cssesc';
import jsStringEscape from 'js-string-escape';
//import urlencode from 'urlencode';

async function sanitiseNumber(dirtyNumber: number | undefined): Promise<number> {
    if (dirtyNumber) {
        return dirtyNumber;
    }

    return 0;
}

async function sanitiseString(dirtyString: string | undefined): Promise<string> {
    if (dirtyString) {
        dirtyString = stripAnsi(dirtyString);
        dirtyString = escapeHtml(dirtyString);
        dirtyString = cssesc(dirtyString);
        dirtyString = jsStringEscape(dirtyString);

        return dirtyString;
    }

    return '';
}

export {
    sanitiseNumber,
    sanitiseString
}