import type { UserClean } from '@gmp-types/users';

declare global{
    namespace Express {
        interface Request {
            currentUser: UserClean
        }
    }
}

