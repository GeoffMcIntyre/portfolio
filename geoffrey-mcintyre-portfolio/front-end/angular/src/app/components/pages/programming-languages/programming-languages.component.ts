import { Component, OnInit } from '@angular/core';
import { ComponentFunctionsService } from '@gmp-services/component-functions.service';
import { ProgrammingLanguagesService } from './programming-languages.service';

@Component({
  selector: 'app-programming-languages',
  templateUrl: './programming-languages.component.html',
  styleUrls: ['./programming-languages.component.scss']
})
export class ProgrammingLanguagesComponent implements OnInit {
	public title = 'Geoffs Portfolio - Programming Languages';

	private programmingLanguages:any[] = [];
  
	public constructor(
		private componentFunctionsService: ComponentFunctionsService,
		private programmingLanguagesService: ProgrammingLanguagesService
	) {
		componentFunctionsService.setTitle(this.title);
	}

	async ngOnInit(): Promise<void> {
		const data: any[] = await this.programmingLanguagesService.getProgrammingLanguages();

		this.programmingLanguages = data;

		console.log("Programming Languages", this.programmingLanguages);
	}

	// showConfig() {

	// }

}
