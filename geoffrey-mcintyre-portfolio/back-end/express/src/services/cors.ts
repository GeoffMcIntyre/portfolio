import { logger } from '@gmp-debug';

import express, { Request, Response, NextFunction } from 'express';
import cors from 'cors';

const corsWhitelist: string[] = [
    'http://localhost',
    'http://example1.com',
    'http://example2.com'
];

const corsOptions: cors.CorsOptions = {
    allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
    ],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: (requestOrigin: string | undefined, callback: (err: Error | null) => void) => {
        logger.debug('Received request from: ' + requestOrigin);

        if (typeof requestOrigin != 'undefined' && corsWhitelist.indexOf(requestOrigin) !== -1) {
            callback(null);
        } else {
            logger.error('CORS blocked request from: ' + requestOrigin);
            callback(new Error('Not allowed by CORS: ' + requestOrigin));
        }
    },
    preflightContinue: false,
};

async function init(server: express.Application): Promise<void> {
    // enabling CORS for all requests
    //server.use(cors());

    server.options('*', async () => getCors()); 
}

async function getCors(): Promise<(req: Request, res: Response, next: NextFunction) => void> {
    return cors(corsOptions);
}

export {
    init,
    getCors
};