import type { Page } from './pages';
import type { ProgrammingLanguage } from './programming-languages';
import type { Software } from './software';

export interface Project {
	_id: string;
	name: string;
	dates: ProjectDates;
	description: string;
	programmingLanguages: ProgrammingLanguage[];
	software: Software[];
	page?: Page;
}

export interface ProjectDates {
	from: Date;
	to?: Date;
}
