
export interface CollectionProperties {
    collectionName: string;
	collectionSchema: CollectionSchema;
	collectionIndexes: Record<string, unknown>[] | null;
	collectionIndexProperties: CollectionIndexProperties | null;
}

export interface CollectionSchema {
	validator: Record<string, unknown>;
}

export interface CollectionIndexProperties {
    unique: boolean;
    name: string;
}