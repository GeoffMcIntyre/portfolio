import { sanitiseString } from '@gmp-services/sanitisation';

import type { Page, PageClean } from '@gmp-types/pages';

async function sanitisePage(pageDirty: Page): Promise<Page>{
    return {
        _id: pageDirty._id,
        containers: pageDirty.containers
    };
}

async function sanitisePageClean(pageCleanDirty: PageClean): Promise<PageClean>{
    return {
        _id: pageCleanDirty._id,
        containers: pageCleanDirty.containers
    };
}

export {
    sanitisePage,
    sanitisePageClean
}