import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { ComponentFunctionsService } from '@gmp-services/component-functions.service';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { AdsComponent } from '@gmp-c-pages/ads/ads.component';
import { AdsService } from '@gmp-c-pages/ads/ads.service';

import { ProgrammingLanguagesComponent } from '@gmp-c-pages/programming-languages/programming-languages.component';
import { ProgrammingLanguagesService } from '@gmp-c-pages/programming-languages/programming-languages.service';

import { ProjectsComponent } from '@gmp-c-pages/projects/projects.component';
import { ProjectsService } from '@gmp-c-pages/projects/projects.service';

import { ProjectComponent } from '@gmp-c-pages/project/project.component';
import { ProjectService } from '@gmp-c-pages/project/project.service';

import { SoftwareComponent } from '@gmp-c-pages/software/software.component';
import { SoftwareService } from '@gmp-c-pages/software/software.service';

import { ComponentGeneratorDirective } from './directives/component-generator.directive';
import { ParagraphComponent } from './components/general/paragraph/paragraph.component';
import { ContainerComponent } from './components/general/container/container.component';

@NgModule({
    declarations: [
        AppComponent,
        AdsComponent,
        ProgrammingLanguagesComponent,
        ProjectsComponent,
        ProjectComponent,
        SoftwareComponent,
        ComponentGeneratorDirective,
        ParagraphComponent,
        ContainerComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule
    ],
    providers: [
        AdsService,
        ComponentFunctionsService,
        ProgrammingLanguagesService,
        ProjectsService,
        ProjectService,
        SoftwareService,
        Title
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
