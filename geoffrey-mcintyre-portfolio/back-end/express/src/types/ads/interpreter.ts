import type { Ad, AdClean } from '@gmp-types/ads';

async function getAdClean(adDirty: Ad): Promise<AdClean | undefined>{
    if (adDirty) {
        return {
            title: adDirty.title,
            price: adDirty.price,
        };
    }
}

export {
    getAdClean
}