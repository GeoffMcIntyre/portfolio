import { errorHandler, logger, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';
import { DatabaseInterface } from '@gmp-classes/databaseInterface';
import { getDatabaseInterface as getRefreshTokensDatabaseInterface } from './refresh-tokens';

import { bcryptCompare, bcryptHash, jwtSignUser } from '@gmp-services/crypto';
import { getUserClean } from '@gmp-types/users/interpreter';
import { sanitiseUser } from '@gmp-types/users/sanitisation';

import type { Db } from 'mongodb';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from '@gmp-types/database';
import type { RefreshToken } from '@gmp-types/refresh-tokens';
import type { User, UserClean, UserDBEntry } from '@gmp-types/users';

const collectionProperties: CollectionProperties = {
    collectionName: 'users',
    collectionSchema: {
        validator: {
            $jsonSchema: {
                bsonType: "object",
                required: [ "username", "password", "permissionLevel" ],
                additionalProperties: false,
                properties: {
                    _id: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    },
                    username: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    password: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    permissionLevel: {
                        bsonType: "int",
                        description: "must be an int and is required"
                    },
                    firstName: {
                        bsonType: "string",
                        description: "must be a string"
                    },
                    lastName: {
                        bsonType: "string",
                        description: "must be a string"
                    },
                    email: {
                        bsonType: "string",
                        description: "must be a string"
                    }
                }
            }
        }
    },
    collectionIndexes: [
        {
            "username": 1
        }
    ],
    collectionIndexProperties: {
        unique: true,
        name: "query for users"
    }
};

class UsersDatabaseInterface extends DatabaseInterface {
    constructor(db: Db, collectionProperties: CollectionProperties) {
        super(db, collectionProperties);
    }

    /**
     * Private
     */
    
    private async _buildEntry(user: User): Promise<UserDBEntry | undefined> {
        return new Promise<UserDBEntry | undefined>(resolve => {
            return bcryptHash(user.password, async (err: Error, hash: string) => {
                if (err) {
                    errorHandler.catchCryptoError(err);
                } else {
                    resolve({
                        username: user.username,
                        password: hash,
                        permissionLevel: 0,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        email: user.email
                    });
                }
            });
        });
    }

    /**
     * Protected
     */

    /**
    * Clean
    */
    
    protected async _cleanGetResult(dirtyResult: DBGetResult): Promise<DBGetResultClean> {
        const cleanResult: DBGetResultClean = {
            success: dirtyResult.success,
            message: dirtyResult.message
        };

        if (Array.isArray(dirtyResult.data)) {
            const users = (dirtyResult.data as User[]);
            cleanResult.data = [];

            for (let i = 0; i < users.length; i++){
                const userClean = await getUserClean(users[i]);

                if (userClean) {
                    cleanResult.data.push(userClean);
                }
            }
        } else {
            const userClean = await getUserClean((dirtyResult.data as User));

            if (userClean) {
                cleanResult.data = userClean;
            }
        }

        return cleanResult;
    }

    /**
     * Public
     */

    public async getAll(): Promise<DBGetResultClean> {
        return await super.getAll();
    }

    public async getById(id: string): Promise<DBGetResultClean> {
        return await super.getById(id);
    }
    
    public async getByUsername(username: string): Promise<DBGetResultClean> {
        return await super.getByUsername(username);
    }
    
    public async insert(user: User): Promise<DBInsertResultClean> {
        user = await sanitiseUser(user);

        const existing = await this._getByUsername(user.username);
        
        if (existing.success) {
            warningHandler.throwDatabaseWarning(`User \`${user.username}\` already exists`);

            return {
                success: false,
                message: `User \`${user.username}\` already exists`
            };
        }
        
        const userDBEntry = await this._buildEntry(user);

        if (userDBEntry) {
            return await super.insert(userDBEntry);
        }

        return {
            success: false,
            message: 'Not Inserted'
        };
    }
    
    public async insertMultiple(users: User[]): Promise<DBInsertResultClean> {
        return await super.insertMultiple(users);
    }
    
    public async update(id: string, user: User): Promise<DBUpdateResultClean> {
        user = await sanitiseUser(user);

        return await super.update(id, user);
    }
    
    public async remove(id: string): Promise<DBDeleteResultClean> {
        return await super.remove(id);
    }

    /**
     * Security
     */
    
    public async login(username: string, password: string): Promise<DBGetResultClean> {
        const userRequest = await this._getByUsername(username);

        if (userRequest.success && userRequest.data && (userRequest.data as User)) {
            const user = (userRequest.data as User);

            return new Promise<DBGetResultClean>(resolve => {
                return bcryptCompare(password, user.password, async (err: Error, match: boolean) => {
                    if (match){
                        const userClean: UserClean = {
                            username: user.username,
                            permissionLevel: user.permissionLevel,
                            firstName: user.firstName,
                            lastName: user.lastName,
                            email: user.email
                        };

                        try {
                            const token = await jwtSignUser(userClean);

                            const refreshToken: RefreshToken = {
                                username: username,
                                token: token.refreshToken
                            };

                            await getRefreshTokensDatabaseInterface().then(async (refreshTokensDatabaseInterface) => {
                                await refreshTokensDatabaseInterface.insert(refreshToken);
                            });
    
                            resolve({
                                success: true,
                                message: 'Login Successful',
                                data: token
                            });
                        } catch (err) {
                            errorHandler.catchCryptoError(err);
                        }
                    }else if (err) {
                        errorHandler.catchCryptoError(err);
                    }
    
                    resolve({
                        success: false,
                        message: 'Login Failed'
                    });
                });
            });
        }

        return {
            success: false,
            message: 'Login Failed'
        };
    }
}

let databaseInterface: UsersDatabaseInterface;

async function init(): Promise<void> {
    databaseInterface = new UsersDatabaseInterface(await db.getUserAccountsDb(), collectionProperties);
}

async function getDatabaseInterface(): Promise<UsersDatabaseInterface> {
    if (!databaseInterface) await init();

    return databaseInterface;
}

export {
    UsersDatabaseInterface,
    getDatabaseInterface
};