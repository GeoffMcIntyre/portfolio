import { errorHandler } from '@gmp-debug';
import fs from 'fs';

function read(secretNameAndPath: string): string | undefined {
    try {
        return fs.readFileSync(`${secretNameAndPath}`, 'utf8');
    } catch (err) {
        if (err.code !== 'ENOENT') {
            errorHandler.catchError(new errorHandler.AppError(`An error occurred while trying to read the secret: ${secretNameAndPath}. Err: ${err}`));
        } else {
            errorHandler.catchError(new errorHandler.AppError(`Could not find the secret, probably not running in swarm mode: ${secretNameAndPath}. Err: ${err}`));
        }
    }
}

export{
    read
}