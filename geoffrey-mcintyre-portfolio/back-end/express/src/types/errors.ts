export interface NodeError {
	name: string;
	message: string;
}