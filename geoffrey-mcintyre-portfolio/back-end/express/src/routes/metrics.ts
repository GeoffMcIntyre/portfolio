import { NextFunction, Request, Response, Router } from 'express';

import * as routeController from '@gmp-controllers/metrics';

import { Route } from '@gmp-classes/route';

async function init(router: Router): Promise<void> {
    const route = new Route(router, 'metrics');

    route.addGet({
        authenticationOptions: {
            disabled: true
        },
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.getAll(req, res, next)
    });
}

export {
    init
};