import { errorHandler, logger, warningHandler } from '@gmp-debug';

import * as db from '@gmp-mongo';
import { DatabaseInterface } from '@gmp-classes/databaseInterface';
import { getProgrammingLanguageClean } from '@gmp-types/programming-languages/interpreter';
import { sanitiseProgrammingLanguage } from '@gmp-types/programming-languages/sanitisation';

import type { Db } from 'mongodb';
import type { CollectionProperties } from '@gmp-types/mongo';
import type { DBGetResult, DBGetResultClean, DBInsertResult, DBInsertResultClean, DBUpdateResult, DBUpdateResultClean, DBDeleteResult, DBDeleteResultClean } from '@gmp-types/database';
import type { ProgrammingLanguage, ProgrammingLanguageClean } from '@gmp-types/programming-languages';

const collectionProperties: CollectionProperties = {
    collectionName: 'programming-languages',
    collectionSchema: {
        validator: {
            $jsonSchema: {
                bsonType: "object",
                required: [ "name" ],
                additionalProperties: false,
                properties: {
                    _id: {
                        bsonType: "objectId",
                        description: "must be a objectId if the field exists"
                    },
                    name: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    },
                    description: {
                        bsonType: "string",
                        description: "must be a string and is required"
                    }
                }
            }
        }
    },
    collectionIndexes: [
        {
            "name": 1
        }
    ],
    collectionIndexProperties: {
        unique: true,
        name: "query for programming languages"
    }
};

class ProgrammingLanguagesDatabaseInterface extends DatabaseInterface {
    constructor(db: Db, collectionProperties: CollectionProperties) {
        super(db, collectionProperties);
    }

    /**
     * Protected
     */

    /**
    * Clean
    */
    
    protected async _cleanGetResult(dirtyResult: DBGetResult): Promise<DBGetResultClean> {
        const cleanResult: DBGetResultClean = {
            success: dirtyResult.success,
            message: dirtyResult.message
        };

        if (Array.isArray(dirtyResult.data)) {
            const programmingLanguages = (dirtyResult.data as ProgrammingLanguage[]);
            cleanResult.data = [];

            for (let i = 0; i < programmingLanguages.length; i++){
                const programmingLanguageClean = await getProgrammingLanguageClean(programmingLanguages[i]);

                if (programmingLanguageClean) {
                    cleanResult.data.push(programmingLanguageClean);
                }
            }
        } else {
            const programmingLanguageClean = await getProgrammingLanguageClean((dirtyResult.data as ProgrammingLanguage));

            if (programmingLanguageClean) {
                cleanResult.data = programmingLanguageClean;
            }
        }

        return cleanResult;
    }

    /**
     * Public
     */

    public async getAll(): Promise<DBGetResultClean> {
        return await super.getAll();
    }

    public async getById(id: string): Promise<DBGetResultClean> {
        return await super.getById(id);
    }
    
    public async getByName(name: string): Promise<DBGetResultClean> {
        return await super.getByName(name);
    }
    
    public async insert(programmingLanguage: ProgrammingLanguage, forceUpdate?: boolean): Promise<DBInsertResultClean> {
        programmingLanguage = await sanitiseProgrammingLanguage(programmingLanguage);

        const existing = await this._getByName(programmingLanguage.name);
        
        if ((existing.data as ProgrammingLanguage)) {
            const id = (existing.data as ProgrammingLanguage)._id;

            if (forceUpdate && id) {
                return await this.update(id, programmingLanguage);
            }

            warningHandler.throwDatabaseWarning(`Programming Language \`${programmingLanguage.name}\` already exists`);

            return {
                success: false,
                message: `Programming Language \`${programmingLanguage.name}\` already exists`
            };
        }

        return await super.insert(programmingLanguage);
    }
    
    public async insertMultiple(programmingLanguages: ProgrammingLanguage[], forceUpdate?: boolean): Promise<DBInsertResultClean> {
        return await super.insertMultiple(programmingLanguages, forceUpdate);
    }
    
    public async update(id: string, programmingLanguage: ProgrammingLanguage): Promise<DBUpdateResultClean> {
        programmingLanguage = await sanitiseProgrammingLanguage(programmingLanguage);

        return await super.update(id, programmingLanguage);
    }
    
    public async remove(id: string): Promise<DBDeleteResultClean> {
        return await super.remove(id);
    }
}

let databaseInterface: ProgrammingLanguagesDatabaseInterface;

async function init(): Promise<void> {
    databaseInterface = new ProgrammingLanguagesDatabaseInterface(await db.getSiteComponentsDb(), collectionProperties);
}

async function getDatabaseInterface(): Promise<ProgrammingLanguagesDatabaseInterface> {
    if (!databaseInterface) await init();

    return databaseInterface;
}

export {
    ProgrammingLanguagesDatabaseInterface,
    getDatabaseInterface
};