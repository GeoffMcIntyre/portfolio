import { DatabaseInterface } from "@gmp-classes/databaseInterface";

export interface DBDataToLoad {
    defaultData: Record<string, unknown>[] | null;
    interface: DatabaseInterface;
}