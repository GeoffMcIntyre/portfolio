import { logger } from '@gmp-debug';

import assert from 'assert';
import { MongoClient, Db, ObjectID } from 'mongodb';

import * as initDatabases from '@gmp-mongo/init';
import * as config from '@gmp-config';

import type { ConfigDatabase } from '@gmp-types/config';

let siteComponentsDb:Db;
let userAccountsDb: Db;
let userGeneratedDb: Db;
let initialised = false;

async function startAll(): Promise<void> {
    logger.info('Starting databases...', initialised);
    initialised = true;

    siteComponentsDb = await startDb(config.get().databases.mongo?.siteComponents);
    userAccountsDb = await startDb(config.get().databases.mongo?.userAccounts);
    userGeneratedDb = await startDb(config.get().databases.mongo?.userGenerated);

    await initDatabases.load();
}

async function startDb(config: ConfigDatabase | undefined): Promise<Db> {
    const dbName = config?.name;
    const dbUrl = config?.url;

    logger.info('Starting database...', dbName, dbUrl);

    if (dbName && dbUrl) {
        //const mongo = new MongoMemoryServer();
        const connection = await MongoClient.connect(dbUrl + '/' + dbName, 
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );
        
        return connection.db(dbName);
        
        //if admin
        //test();
    } else {
        throw(new Error('Can\'t connect to MongoDB'));
    }
}

async function getSiteComponentsDb(): Promise<Db> {
    if (!siteComponentsDb) await startAll();
    return siteComponentsDb;
}

async function getUserAccountsDb(): Promise<Db> {
    if (!userAccountsDb) await startAll();
    return userAccountsDb;
}

async function getUserGeneratedDb(): Promise<Db> {
    if (!userGeneratedDb) await startAll();
    return userGeneratedDb;
}

async function test(database: Db): Promise<void> {
    logger.debug('Running database tests...');

    // List all the available databases
    const adminDb = database.admin();

    await adminDb.listDatabases(function(err, dbs) {
        assert.equal(null, err);
        assert.ok(dbs.databases.length > 0);

        for (let i = 0; i < dbs.databases.length; i++){
            logger.debug('Database information: ' + JSON.stringify(dbs.databases[i]));
        }
    });
}

export {
    getSiteComponentsDb,
    getUserAccountsDb,
    getUserGeneratedDb,
    startAll,
    ObjectID
};