import { Injectable } from '@angular/core';
import { DataService } from '@gmp-services/data.service';
import { Observable, of } from 'rxjs';

@Injectable()
export class AdsService {
    private adsConfigUrl = 'assets/config.json';

    constructor(private dataService: DataService) { }
	
    async getAds(): Promise<any[]>{
      return await this.dataService.sendGetRequest('ads');
    }
}