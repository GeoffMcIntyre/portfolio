const messageHighlight = 'Geoff\'s Portfolio';

async function debug(message?: string | undefined, ...optionalParams: any[]): Promise<void> {
    if (optionalParams.length > 0) {
        console.log(messageHighlight + ' Debug: ' + message, optionalParams);
    } else {
        console.log(messageHighlight + ' Debug: ' + message);
    }
}

async function info(message?: string | undefined, ...optionalParams: any[]): Promise<void> {
    if (optionalParams.length > 0) {
        console.info(messageHighlight + ' Information: ' + message, optionalParams);
    } else {
        console.info(messageHighlight + ' Information: ' + message);
    }
}

async function warn(message?: string | undefined, ...optionalParams: any[]): Promise<void> {
    if (optionalParams.length > 0) {
        console.warn(messageHighlight + ' Warning: ' + message, optionalParams);
    } else {
        console.warn(messageHighlight + ' Warning: ' + message);
    }
}

async function error(message?: string | undefined, ...optionalParams: any[]): Promise<void> {
    if (optionalParams.length > 0) {
        console.error(messageHighlight + ' Error: ' + message, optionalParams);
    } else {
        console.error(messageHighlight + ' Error: ' + message);
    }
}

export {
    debug,
    error,
    info,
    warn
};