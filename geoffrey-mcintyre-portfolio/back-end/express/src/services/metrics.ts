import { logger } from '@gmp-debug';

import promClient from 'prom-client';

const prefix = 'gmp_backend_express_api:';

let promClientRegistry:promClient.Registry;

async function start(): Promise<void> {
    logger.info('Starting prom client...');

    // enable prom-client to expose default application metrics
    const collectDefaultMetrics = promClient.collectDefaultMetrics;
    
    // define a custom prefix string for application metrics
    collectDefaultMetrics({ prefix: prefix });
    
    // a custom histogram metric which represents the latency
    // of each call to our API /api/greeting.
    // const histogram = new promClient.Histogram({
    //     name: 'my_application:hello_duration',
    //     help: 'Duration of HTTP requests in ms',
    //     labelNames: ['method', 'status_code'],
    //     buckets: [0.1, 5, 15, 50, 100, 500]
    // });

    promClientRegistry = promClient.register;
}

async function getPromClientRegister(): Promise<promClient.Registry> {
    if (!promClientRegistry) await start();
    return promClientRegistry;
}

export {
    start,
    getPromClientRegister
}