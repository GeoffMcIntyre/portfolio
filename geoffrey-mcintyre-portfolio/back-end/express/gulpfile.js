const gulp = require('gulp');

const defaultTask = (cb) => {
    console.log(`hello world!`);
    cb();
};

const watchAll = () => {
    //   If you want to run the transpileSassToCss function when
    // multiple files are changed, put the file paths in the array
    return gulp.watch(['src/sass/**/*.scss'], transpileSassToCss)
    //   This glob tells gulp to watch all the files suffixed with .scss
    //   in the sass folder & in folders in the sass folder
}

exports.default = defaultTask;
exports.watch = watchAll;