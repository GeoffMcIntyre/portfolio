import { Injectable } from '@angular/core';
import { DataService } from '@gmp-services/data.service';
import { Observable, of } from 'rxjs';

import type { Project } from '@gmp-types/projects';

@Injectable()
export class ProjectService {

	constructor(private dataService: DataService) { }
	
	async getProject(projectId: string): Promise<Project>{
		return await this.dataService.sendGetRequest('projects/' + projectId);
	}
}