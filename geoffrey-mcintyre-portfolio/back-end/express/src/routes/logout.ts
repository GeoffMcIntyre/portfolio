import { NextFunction, Request, Response, Router } from 'express';

import { getRouteController } from '@gmp-controllers/refresh-tokens';

import { Route } from '@gmp-classes/route';

async function init(router: Router): Promise<void> {
    const route = new Route(router, 'logout');
    const routeController = await getRouteController();
    
    route.addPost({
        authenticationOptions: {
            disabled: true
        },
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.logout(req, res, next)
    });
    
    route.addPost({
        path: 'all',
        authenticationOptions: {
            disabled: true
        },
        cacheOptions: {
            ageSeconds: 0
        },
        controller: async (req: Request, res: Response, next: NextFunction) => routeController.logoutAll(req, res, next)
    });
}

export {
    init
};