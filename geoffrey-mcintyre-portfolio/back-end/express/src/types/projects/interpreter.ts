import { getPageClean } from '@gmp-types/pages/interpreter';
import type { Project, ProjectClean, ProjectDBEntry } from '@gmp-types/projects';

async function getProjectClean(projectDirty: Project): Promise<ProjectClean | undefined>{
    if (projectDirty) {
        return {
            prettyId: projectDirty.prettyId,
            name: projectDirty.name,
            dates: projectDirty.dates,
            description: projectDirty.description,
            programmingLanguages: projectDirty.programmingLanguages,
            software: projectDirty.software,
            page: projectDirty.page ? await getPageClean(projectDirty.page) : undefined
        };
    }
}

async function getProjectDBEntryClean(projectDirty: ProjectDBEntry): Promise<ProjectClean | undefined>{
    if (projectDirty) {
        return {
            prettyId: projectDirty.prettyId,
            name: projectDirty.name,
            dates: projectDirty.dates,
            description: projectDirty.description,
            programmingLanguages: [],
            software: []
        };
    }
}

export {
    getProjectClean,
    getProjectDBEntryClean
}