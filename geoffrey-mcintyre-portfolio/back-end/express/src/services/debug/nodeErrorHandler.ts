import type { NodeError } from '@gmp-types/errors';

async function listen(): Promise<any> {
    process.on('unhandledRejection', (err: NodeError) => {
        if (err && 'name' in err && 'message' in err) {
            console.log(err.name, err.message);
        }

        console.log('UNHANDLED REJECTION! 💥 Shutting down...');
        
        process.exit(1);
    });

    process.on('uncaughtException', (err: NodeError) => {
        if (err && 'name' in err && 'message' in err) {
            console.log(err.name, err.message);
        }
        
        console.log('UNHANDLED EXCEPTION! 💥 Shutting down...');
        
        process.exit(1);
    });
}

export {
    listen
};