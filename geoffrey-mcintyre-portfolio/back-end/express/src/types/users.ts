export interface User {
	_id?: string;
    username: string;
    password: string;
    permissionLevel: number;
    firstName?: string;
    lastName?: string;
    email?: string;
}

export interface UserClean {
    username: string;
    permissionLevel: number;
    firstName?: string;
    lastName?: string;
    email?: string;
}

export interface DBGetUserResult {
    success: boolean;
    message: string;
	data: UserClean[] | UserClean;
}

export interface UserDBEntry {
	_id?: string;
    username: string;
    password: string;
    permissionLevel: number;
    firstName?: string;
    lastName?: string;
    email?: string;
}