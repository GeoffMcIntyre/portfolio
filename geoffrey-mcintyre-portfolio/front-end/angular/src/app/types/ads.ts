export interface Ad {
	_id: string;
	title: string;
	price: number;
}