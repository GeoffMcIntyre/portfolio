import { logger } from '@gmp-debug';

import { Db } from 'mongodb';

import { getDatabaseInterface as getAdsDatabaseInterface } from './ads';
import { getDatabaseInterface as getProgrammingLanguagesDatabaseInterface } from './programming-languages';
import { getDatabaseInterface as getProjectPagesDatabaseInterface } from './project-pages';
import { getDatabaseInterface as getProjectsDatabaseInterface } from './projects';
import { getDatabaseInterface as getSoftwareDatabaseInterface } from './software';
import { getDatabaseInterface as getUsersDatabaseInterface } from './users';

// Initial Data
import adsData from '@gmp-data/ads.json';
import programmingLanguagesData from '@gmp-data/programming-languages.json';
import projectsData from '@gmp-data/projects.json';
import softwareData from '@gmp-data/software.json';
import usersData from '@gmp-data/users.json';

import type { DBDataToLoad } from '@gmp-types/dataLoad';
import type { CollectionProperties } from '@gmp-types/mongo';

const forceDataReset = true;

async function getDataToLoad(): Promise<DBDataToLoad[]> {
    return [
        {
            defaultData: adsData,
            interface: await getAdsDatabaseInterface()
        },
        {
            defaultData: programmingLanguagesData,
            interface: await getProgrammingLanguagesDatabaseInterface()
        },
        {
            defaultData: null,
            interface: await getProjectPagesDatabaseInterface()
        },
        {
            defaultData: projectsData,
            interface: await getProjectsDatabaseInterface()
        },
        {
            defaultData: softwareData,
            interface: await getSoftwareDatabaseInterface()
        },
        {
            defaultData: usersData,
            interface: await getUsersDatabaseInterface()
        }
    ];
}

async function drop(database:Db, collectionName:CollectionProperties["collectionName"]): Promise<void> {
    logger.debug('Database collection `' + collectionName + '` dropping indexes.');
    await database.collection(collectionName).dropIndexes();

    logger.debug('Database collection `' + collectionName + '` dropping collection.');
    await database.collection(collectionName).drop(function(err, success) {
        if (err) throw err;
        if (success) console.log("Collection deleted");
    });
}

async function init(
    database: Db,
    collectionName: CollectionProperties["collectionName"],
    collectionSchema: CollectionProperties["collectionSchema"],
    collectionIndexes: CollectionProperties["collectionIndexes"],
    collectionIndexProperties: CollectionProperties["collectionIndexProperties"]
): Promise<void> {
    logger.debug('Database collection `' + collectionName + '` being created.');
    await database.createCollection(collectionName, collectionSchema);

    if (collectionIndexes && collectionIndexProperties) {
        for (let i = 0; i < collectionIndexes.length; i++){
            logger.debug('Database collection `' + collectionName + '` creating index.');
            await database.collection(collectionName).createIndex(collectionIndexes[i], collectionIndexProperties);
        }
    }
}

async function load(): Promise<void> {
    logger.debug('Loading initial database data...');

    const dataToLoad = await getDataToLoad();

    for (let i = 0; i < dataToLoad.length; i++){
        const database = await dataToLoad[i].interface.getDatabase();
        const collections = await database.listCollections().toArray();
        
        for (let j = 0; j < collections.length; j++) {
            
            if ('name' in collections[j]) {
                const collectionName: string = collections[j]['name'];

                if (dataToLoad[i].interface.getCollectionProperties().collectionName == collectionName) {
                    logger.debug('Database collection found `' + collectionName + '`.');

                    if (forceDataReset) {
                        logger.debug('Forcing data reset...');
                        await drop(database, collectionName);
                    } else {
                        dataToLoad.splice(i, 1);
                    }

                    break;
                }
            }
        }
    }

    for (let i = 0; i < dataToLoad.length; i++){
        await init(dataToLoad[i].interface.getDatabase(),
            dataToLoad[i].interface.getCollectionProperties().collectionName,
            dataToLoad[i].interface.getCollectionProperties().collectionSchema,
            dataToLoad[i].interface.getCollectionProperties().collectionIndexes,
            dataToLoad[i].interface.getCollectionProperties().collectionIndexProperties);
    }

    for (let i = 0; i < dataToLoad.length; i++){

        if (dataToLoad[i].defaultData) {
            logger.debug('Database collection `' + dataToLoad[i].interface.getCollectionProperties().collectionName + '` default data being loaded: ' + JSON.stringify(dataToLoad[i].defaultData));
            await dataToLoad[i].interface.insertMultiple(dataToLoad[i].defaultData, true);
        }
    }
}

export {
    load
};