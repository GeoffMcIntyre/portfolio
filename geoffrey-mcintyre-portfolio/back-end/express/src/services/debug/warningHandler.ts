import * as logger from '@gmp-debug/logger';

const logWarningDev = (message: string) => {
    logger.warn(message);
};

const logWarningProd = (message: string) => {
    logger.warn(message);
};

async function catchWarning(message: string): Promise<void> {
    if (process.env.NODE_ENV === 'development') {
        logWarningDev(message);
    } else if (process.env.NODE_ENV === 'production') {
        logWarningProd(message);
    }
}

async function throwDatabaseWarning(message: string): Promise<void> {
    catchWarning(`Database warning: ${message}`);
}

export {
    catchWarning,
    throwDatabaseWarning
};