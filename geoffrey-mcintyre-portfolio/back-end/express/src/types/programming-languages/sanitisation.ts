import { sanitiseString } from '@gmp-services/sanitisation';

import type { ProgrammingLanguage } from '@gmp-types/programming-languages';

async function sanitiseProgrammingLanguage(programmingLanguageDirty: ProgrammingLanguage): Promise<ProgrammingLanguage>{
    return {
        name: await sanitiseString(programmingLanguageDirty.name),
        description: await sanitiseString(programmingLanguageDirty.description)
    };
}

async function sanitiseProgrammingLanguages(programmingLanguageDirty: ProgrammingLanguage[]): Promise<ProgrammingLanguage[]>{
    const programmingLanguageClean: ProgrammingLanguage[] = [];

    for (let i = 0; i < programmingLanguageDirty.length; i++){
        programmingLanguageClean.push({
            name: await sanitiseString(programmingLanguageDirty[i].name),
            description: await sanitiseString(programmingLanguageDirty[i].description)
        })
    }

    return programmingLanguageClean;
}

export {
    sanitiseProgrammingLanguage,
    sanitiseProgrammingLanguages
}