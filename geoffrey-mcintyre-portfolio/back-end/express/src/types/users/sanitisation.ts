import { sanitiseNumber, sanitiseString } from '@gmp-services/sanitisation';

import type { User } from '@gmp-types/users';

async function sanitiseUser(userDirty: User): Promise<User>{
    return {
        username: await sanitiseString(userDirty.username),
        password: await sanitiseString(userDirty.password),
        permissionLevel: await sanitiseNumber(userDirty.permissionLevel),
        firstName: await sanitiseString(userDirty.firstName),
        lastName: await sanitiseString(userDirty.lastName),
        email: await sanitiseString(userDirty.email)
    };
}

export {
    sanitiseUser
}